# NYC High Schools Demo App

## Overview

This is an Android demo app listing all NYC high schools with their SAT scores.

## Run

To run the project on a device, run `./project.sh run`.

Note that this project uses the [Facebook Buck](https://buck.build/) build system.

To run the project, Facebook Buck must be installed.

## Install (alternative)

Download the APK from Dropbox [here](https://www.dropbox.com/s/ghe56hulfluis1e/app-aligned.apk?dl=0). (Please note: GitHub does not allow storing APKs.)

Then run `adb install app-aligned.apk`.

## My tech approach

I used Retrofit for the HTTP client because I like how it separates the API from the code.

I seperate all the HTTP logic from the UI logic to keep the code clean, making it easier to call endpoints throughout the UI and managed serializable data from one place.

In the UI, I keep everything clean and aligned with the Android framework, which means keeping all the resources in the correct files and getting them from the application's components. I also separate all independent logic (e.g. REST API, API caller, utility and constant code) among individual classes.

## Authors

* **Adrian Bartyczak** - *Entire project* - [Adrian B. - UpWork](https://www.upwork.com/freelancers/~01b4a9df6fbdba292d)
