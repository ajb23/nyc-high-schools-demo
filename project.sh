#!/usr/bin/env bash
# 
# File:
#   project.sh
# 
# Description:
#   The project management script for the NYC High Schools demo app
# 

readonly HERE="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
readonly APP_PACKAGE_NAME='com.nychighschoolsdemo.app'
readonly BUCK_APP_BINARY_BUILD_NAME='app'
readonly OPEN_EMULATOR_LOG=false

printHelpMessage() {
echo -e "\
project.sh help documentation

Usage:
  project.sh [options] <action> [action arguments]

Options:
  -h    show this help message

Actions:
  clean          clean the app
  build          build the app
  resolve        resolve the app's dependencies
  run            run the app on a device or emulator
  log            open up logcat in terminal
  clear          clear the app's data
  launch | l     launch the app
  uninstall      uninstall the app from the device
"
}

# ========================================================
#   Process the options and argument
# ========================================================

while getopts :h OPT; do
	case "${OPT}" in
		h)  printHelpMessage
				exit
				;;
		\?) echo "project.sh: option \"-${OPTARG}\" unknown" 1>&2
				exit 1
				;;
	esac
done

if [ $# -lt 1 ]; then
	echo 'project.sh: invalid number of arguments; see help' 1>&2
	exit 1
else
	ACTION="${1}"
	shift
fi

# ========================================================
#   Utility functions
# ========================================================

# Update the dependency directory of the IDE.
# Arguments:
#   1. (Optional) Root path of project directory
#   2. BUILD targets to recusively resolve dependencies for
#   3. Module subdirectory within the main jars directory
updateIDEDependencyDirectory() {
	for modl in $(find modules/ -maxdepth 1 -type d -printf '%P\n'); do
		if [ -d .out/ide_deps/"${modl}" ]; then
			rm -Rf .out/ide_deps/"${modl}"/*
		else
			mkdir -p .out/ide_deps/"${modl}"
		fi
		for ivyJarFile in $(find .pants.d/ivy/jars/ -name '*.jar'); do
			cp -R "${ivyJarFile}" .out/ide_deps/"${modl}"
		done
	done
}

# ========================================================
#   Process the action
# ========================================================

cd "${HERE}"

case "${ACTION}" in
  'setup')
      if ! iswindopen 'main .* - IntelliJ IDEA'; then
        runinbg idea "${HERE}"
        sleep 8
        wmctrlregex -r 'main .* - IntelliJ IDEA' -e 0,0,-1,-1,-1
        wmctrlregex -r 'main .* - IntelliJ IDEA' -b \
              add,maximized_vert,maximized_horz
      fi
      ;;
  'clean')
    buck clean
    ;;
  'build')
    buck build "${BUCK_APP_BINARY_BUILD_NAME}"
    # This aligns the generated APK for the app store.
    if [ -f buck-out/gen/app.signed.apk ]; then
      algndApkFileName='app-aligned.apk'
      algndApkOutPath="${HERE}/${algndApkFileName}"
      echo -e "::Creating aligned APK \"${algndApkFileName}\"."
      /usr/local/lib/android-sdk/build-tools/24.0.0/zipalign -f 4 \
            buck-out/gen/app.signed.apk "${algndApkOutPath}"
    fi
    ;;
  'run')
    if [ "$(du -cksm buck-out/ | cut -f 1 | head -1)" -gt 350 ]; then
      rm -Rf buck-out/*
    fi
    buck install --run "${BUCK_APP_BINARY_BUILD_NAME}" --all-devices
    ;;
  'resolve')
    cd deps/
    ant copy-external resolve install set-up-ide-libs
    ;;
  'log')
    if [[ "$#" -eq 0 || "${1}" = 'new' ]]; then
      windName='logcat-all'
      windLoc='100,100,1100,850'
    else
      windName='logcat-other'
      windLoc='30,150,1100,850'
    fi
    if iswindopen "${windName}" && [ ! "${1}" = 'new' ]; then
      wmctrl -a "${windName}"
    else
      wmctrl -c "${windName}"
      wmctrl -c "${windName}-emulator"
      if [ "${windName}" = 'logcat-other' ]; then
        if [ "${1}" = 'all' ]; then
          cmd="sleep .2; pidcat -d"
        else
          cmd="adb -d logcat -s \"${1}\""
        fi
      else
        # cmd="adb logcat | proclogcat ${APP_PACKAGE_NAME}"
        cmd="sleep .2; pidcat -e ${APP_PACKAGE_NAME}"
      fi
      adb logcat -c
      newterm -g '100x44+120+100' -t "${windName}" "${cmd}"
      
      if [ "${OPEN_EMULATOR_LOG}" = 'true' ]; then
        newterm -g '100x44+120+100' -t "${windName}-emulator" \
              "sleep .2; pidcat -e ${APP_PACKAGE_NAME}"
        sleep .2
        wmctrl -r "${windName}" -e 0,"${windLoc}"
      fi
    fi
    ;;
  'clear' | 'c')
    wind -a minimize "${USER}@${HOST}"
    adb -d shell pm clear "${APP_PACKAGE_NAME}"
    # adb -e shell pm clear "${APP_PACKAGE_NAME}"
    ;;
  'launch' | 'l')
    sleep .2
    adb -d shell monkey -p "${APP_PACKAGE_NAME}" -c android.intent.category.LAUNCHER 1 >/dev/null
    # adb -e shell monkey -p "${APP_PACKAGE_NAME}" -c android.intent.category.LAUNCHER 1 >/dev/null

    sleep 2
    "${MOBILE_INPUT_SCRIPT}" press_button_allow
    ;;
  'uninstall')
    # buck uninstall app
    adb uninstall "${APP_PACKAGE_NAME}"
    ;;
  *)
    echo "project.sh: action \"${ACTION}\" does not exist" 1>&2
    ;;
esac
