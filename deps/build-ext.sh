#!/bin/bash
# 
# Build script used as an extension for build.xml.
# 

readonly HERE="$(dirname "$(readlink -f "$0")")"
readonly IDE_LIBS_DIR="${HERE}/libs/ide"
readonly IVY_LIBS_DIR="${HERE}/libs/ivy"
readonly STATIC_LIBS_DIR="${HERE}/libs/static"
readonly ANDROID_SUPPORT_LIBS_DIR="${HERE}/project-libs/android-support/23.4.0"
readonly GOOGLE_SUPPORT_LIBS_DIR="${HERE}/project-libs/google-support/11.0.4"

if [ $# -lt 1 ]; then
  echo 'build-ext: invalid number of arguments'
  exit
fi

# ========================================================
#   Process the necessary action
# ========================================================

case "${1}" in
  'extract-aar-classes')
    cd "${IDE_LIBS_DIR}"/

    for aar in "${IVY_LIBS_DIR}"/*.aar "${STATIC_LIBS_DIR}"/*.aar \
        "${ANDROID_SUPPORT_LIBS_DIR}"/*.aar \
        "${GOOGLE_SUPPORT_LIBS_DIR}"/*.aar; do
      echo $aar
      fileBaseName="$(echo "${aar}" | sed 's/.*\///' | sed 's/\.[^.]*$//')"
      jar xf "${aar}" 'classes.jar'
      mv 'classes.jar' "${fileBaseName}.jar"
    done
    ;;

  *)
    echo "build-ext: target \"${TARGET}\" does not exist"
    ;;
esac
