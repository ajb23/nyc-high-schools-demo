# 
# App BUCK file
# -------------------------------------------------------------

# -------------------------------------------------------------
#   Libraries
# -------------------------------------------------------------

import re
include_defs('//DEFS')

for jarfile in glob(['deps/libs/ivy/*.jar', 'deps/libs/static/*.jar', 'deps/repo/jars/*.jar']):
  name = 'jars__' + re.sub(r'^.*/([^/]+)\.jar$', r'\1', jarfile)
  jar_deps.append(':' + name)
  prebuilt_jar(
    name = name,
    binary_jar = jarfile,
  )

# Presto libraries: allows all java class and res dependencies to be imported with a single target
presto_gen_aidls = []
for aidlfile in glob(['src/com/aocate/presto/service/*.aidl']):
  name = 'presto_aidls__' + re.sub(r'^.*/([^/]+)\.aidl$', r'\1', aidlfile)
  presto_gen_aidls.append(':' + name)
  gen_aidl(
    name = name,
    aidl = aidlfile,
    import_path = 'src',
  )

for andr_preblt_aar in ANDROID_PREBUILT_AARS:
  android_prebuilt_aar(name = andr_preblt_aar['name'], aar = andr_preblt_aar['aar'])

android_library(**ALL_JARS_PARAMS)

android_library(name = 'presto-aidls', srcs = presto_gen_aidls)

PRESTO_LIB_PARAMS['srcs'] = glob(PRESTO_LIB_PARAMS['srcs'])
android_library(**PRESTO_LIB_PARAMS)

# -------------------------------------------------------------
#   Resources and Manifest
# -------------------------------------------------------------

for resource in RESOURCES:
  android_resource(name = resource['name'], package = resource['package'], res = resource['res'], assets = resource['assets'], deps = resource['deps'])

android_manifest(**MANIFEST_PARAMS)

# -------------------------------------------------------------
#   External Resources
# -------------------------------------------------------------

export_file(**EXTR_GOOGLE_SERVICES_JSON_PARAMS)

# -------------------------------------------------------------
#   Sources
# -------------------------------------------------------------

SRC_LIB_PARAMS['srcs'] = glob(SRC_LIB_PARAMS['srcs'], excludes = [APP_CLASS_SOURCE])
android_library(**SRC_LIB_PARAMS)

# -------------------------------------------------------------
#   Binaries
# -------------------------------------------------------------

android_binary(**APP_BINARY_PARAMS)

android_library(**APPLICATION_LIB_PARAMS)

android_binary(**APP_BINARY_EXO_PARAMS)

# -------------------------------------------------------------
#   Other
# -------------------------------------------------------------

android_build_config(**BUILD_CONFIG_PARAMS)

keystore(**KEYSTORE_PARAMS)
