package com.nychighschoolsdemo.app.model;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.nychighschoolsdemo.app.util.SerializeUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * The high school serializer
 */
public class HighSchoolSerializer extends JsonSerializer<HighSchool> {
    private static final Logger LOGGER = LoggerFactory.getLogger(HighSchoolSerializer.class);

    @Override
    public void serialize(HighSchool highSchool, JsonGenerator jsonGenerator,
                          SerializerProvider serializerProvider) throws IOException {
        JSONObject highSchlJsonObje = new JSONObject();
        try {
            highSchlJsonObje.put("dbn", highSchool.getDbn());
            highSchlJsonObje.put("school_name", highSchool.getSchoolName());
            highSchlJsonObje.put("boro", highSchool.getBoro());
            highSchlJsonObje.put("sat_scores", SerializeUtils.serialize(highSchool.getSatScores()));
        } catch (JSONException e) {
            LOGGER.error("Failed to serialize high school: {}", e.toString());
            e.printStackTrace();
        }

        jsonGenerator.writeRawValue(highSchlJsonObje.toString());
    }

}
