package com.nychighschoolsdemo.app.model;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import java.io.IOException;

/**
 * The high school deserializer
 */
public class HighSchoolDeserializer extends StdDeserializer<HighSchool> {

    public HighSchoolDeserializer() {
        this(null);
    }

    public HighSchoolDeserializer(Class<?> vc) {
        super(vc);
    }

    @Override
    public HighSchool deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException {
        JsonNode rootJsonNode = jp.getCodec().readTree(jp);
        String dbn = null;
        String schoolName = null;
        String boro = null;
        if (rootJsonNode.has("dbn")) {
            dbn = rootJsonNode.get("dbn").asText(null);
        }
        if (rootJsonNode.has("school_name")) {
            schoolName = rootJsonNode.get("school_name").asText(null);
        }
        if (rootJsonNode.has("boro")) {
            boro = rootJsonNode.get("boro").asText(null);
        }
        return new HighSchool(dbn, schoolName, boro, null);
    }

}
