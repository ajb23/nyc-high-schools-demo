package com.nychighschoolsdemo.app.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * A high school
 */
@JsonAutoDetect
@JsonSerialize(using = HighSchoolSerializer.class)
@JsonDeserialize(using = HighSchoolDeserializer.class)
public class HighSchool {
    private String mDbn;
    private String mSchoolName;
    private String mBoro;
    private SatScores mSatScores;

    public HighSchool() { }

    public HighSchool(String dbn, String schoolName, String boro, SatScores satScores) {
        mDbn = dbn;
        mSchoolName = schoolName;
        mBoro = boro;
        mSatScores = satScores;
    }

    public String getDbn() {
        return mDbn;
    }

    public void setDbn(String dbn) {
        mDbn = dbn;
    }

    public String getSchoolName() {
        return mSchoolName;
    }

    public void setSchoolName(String schoolName) {
        mSchoolName = schoolName;
    }

    public String getBoro() {
        return mBoro;
    }

    public void setBoro(String boro) {
        mBoro = boro;
    }

    public SatScores getSatScores() {
        return mSatScores;
    }

    public void setSatScores(SatScores satScores) {
        mSatScores = satScores;
    }
}
