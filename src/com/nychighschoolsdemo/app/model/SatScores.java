package com.nychighschoolsdemo.app.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * The high school SAT scores
 */
@JsonAutoDetect
@JsonSerialize(using = SatScoresSerializer.class)
@JsonDeserialize(using = SatScoresDeserializer.class)
public class SatScores {
    private String mDbn;
    private String mSchoolName;
    private int mCriticalReadingAverageScore;
    private int mMathAverageScore;
    private int mWritingAverageScore;

    public SatScores() { }

    public SatScores(String dbn, String schoolName, int criticalReadingAverageScore, int mathAverageScore,
                     int writingAverageScore) {
        mDbn = dbn;
        mSchoolName = schoolName;
        mCriticalReadingAverageScore = criticalReadingAverageScore;
        mMathAverageScore = mathAverageScore;
        mWritingAverageScore = writingAverageScore;
    }

    public String getDbn() {
        return mDbn;
    }

    public void setDbn(String dbn) {
        mDbn = dbn;
    }

    public String getSchoolName() {
        return mSchoolName;
    }

    public void setSchoolName(String schoolName) {
        mSchoolName = schoolName;
    }

    public int getCriticalReadingAverageScore() {
        return mCriticalReadingAverageScore;
    }

    public void setCriticalReadingAverageScore(int criticalReadingAverageScore) {
        mCriticalReadingAverageScore = criticalReadingAverageScore;
    }

    public int getMathAverageScore() {
        return mMathAverageScore;
    }

    public void setMathAverageScore(int mathAverageScore) {
        mMathAverageScore = mathAverageScore;
    }

    public int getWritingAverageScore() {
        return mWritingAverageScore;
    }

    public void setWritingAverageScore(int writingAverageScore) {
        mWritingAverageScore = writingAverageScore;
    }

}
