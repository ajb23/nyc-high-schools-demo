package com.nychighschoolsdemo.app.model;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import java.io.IOException;

/**
 * The SAT scores deserializer
 */
public class SatScoresDeserializer extends StdDeserializer<SatScores> {

    public SatScoresDeserializer() {
        this(null);
    }

    public SatScoresDeserializer(Class<?> vc) {
        super(vc);
    }

    @Override
    public SatScores deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException {
        JsonNode rootJsonNode = jp.getCodec().readTree(jp);
        String dbn = null;
        String schoolName = null;
        int criticalReadingAverageScore = 0;
        int mathAverageScore = 0;
        int writingAverageScore = 0;
        if (rootJsonNode.has("dbn")) {
            dbn = rootJsonNode.get("dbn").asText(null);
        }
        if (rootJsonNode.has("school_name")) {
            schoolName = rootJsonNode.get("school_name").asText(null);
        }
        if (rootJsonNode.has("sat_critical_reading_avg_score")) {
            criticalReadingAverageScore = rootJsonNode.get("sat_critical_reading_avg_score").asInt();
        }
        if (rootJsonNode.has("sat_math_avg_score")) {
            mathAverageScore = rootJsonNode.get("sat_math_avg_score").asInt();
        }
        if (rootJsonNode.has("sat_writing_avg_score")) {
            writingAverageScore = rootJsonNode.get("sat_writing_avg_score").asInt();
        }
        return new SatScores(dbn, schoolName, criticalReadingAverageScore, mathAverageScore,
                writingAverageScore);
    }

}
