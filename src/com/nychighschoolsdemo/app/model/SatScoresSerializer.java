package com.nychighschoolsdemo.app.model;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * The SAT scores serializer
 */
public class SatScoresSerializer extends JsonSerializer<SatScores> {
    private static final Logger LOGGER = LoggerFactory.getLogger(SatScoresSerializer.class);

    @Override
    public void serialize(SatScores satScores, JsonGenerator jsonGenerator,
                          SerializerProvider serializerProvider) throws IOException {
        JSONObject satScoresJsonObje = new JSONObject();
        try {
            satScoresJsonObje.put("dbn", satScores.getDbn());
            satScoresJsonObje.put("school_name", satScores.getSchoolName());
            satScoresJsonObje.put("sat_critical_reading_avg_score",
                    satScores.getCriticalReadingAverageScore());
            satScoresJsonObje.put("sat_math_avg_score", satScores.getMathAverageScore());
            satScoresJsonObje.put("sat_writing_avg_score", satScores.getWritingAverageScore());
        } catch (JSONException e) {
            LOGGER.error("Failed to serialize SAT scores: {}", e.toString());
        }

        jsonGenerator.writeRawValue(satScoresJsonObje.toString());
    }

}
