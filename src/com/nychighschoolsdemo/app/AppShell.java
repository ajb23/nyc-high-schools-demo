package com.nychighschoolsdemo.app;

import com.facebook.buck.android.support.exopackage.ExopackageApplication;

public class AppShell extends ExopackageApplication {
	
    public AppShell() {
        super("com.fencedin.android.base.Application", BuildConfig.EXOPACKAGE_FLAGS);
        //super(1);
    }

}
