package com.nychighschoolsdemo.app.rest;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.*;

public interface EndpointsApi {

    @GET("/resource/s3k6-pzi2.json")
    Call<ResponseBody> getSchoolListRequest();

    @GET("/resource/f9bf-2cp4.json")
    Call<ResponseBody> getSatScoresRequest();

}
