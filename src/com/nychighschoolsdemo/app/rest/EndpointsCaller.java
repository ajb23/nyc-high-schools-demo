package com.nychighschoolsdemo.app.rest;

import android.os.SystemClock;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nychighschoolsdemo.app.base.AppConstants;
import com.nychighschoolsdemo.app.base.AppSharedPreferences.HTTP_REQUEST_DATA;
import com.nychighschoolsdemo.app.base.AppSharedPreferences.HTTP_REQUEST_METADATA;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import static com.nychighschoolsdemo.app.rest.EndpointsCaller.Endpoint.GET_HIGH_SCHOOLS;
import static com.nychighschoolsdemo.app.rest.EndpointsCaller.Endpoint.GET_SAT_SCORES;
import static com.nychighschoolsdemo.app.rest.HttpRequestError.TYPE.FAILED_TO_GET_RESPONSE_BODY;
import static com.nychighschoolsdemo.app.rest.HttpRequestError.TYPE.RESPONSE_BODY_PROCESSING_FAILED;

/**
 * The endpoints caller
 */
public class EndpointsCaller {
    private static EndpointsCaller EndpointsCaller = null;
    private static final Logger LOGGER = LoggerFactory.getLogger(EndpointsCaller.class);
    protected static EndpointsApi ENDPOINTS_API;
    private static Call<okhttp3.ResponseBody> sCurrentCall;
    private static Endpoint sCurrentCallRequestEndpoint;

    public EndpointsCaller() {
        OkHttpClient.Builder clntBldr = new OkHttpClient.Builder();

        // Note: The read timeout should be at least 2 seconds greater than the minimum log out-log in interval
        // in the log in method of the users endpoint class in the backend.
        clntBldr.readTimeout(AppConstants.Http.READ_TIMEOUT, TimeUnit.MILLISECONDS);
        clntBldr.connectTimeout(AppConstants.Http.CONNECTION_TIMEOUT, TimeUnit.MILLISECONDS);

        if (AppConstants.ENABLE_DEVELOPMENT_MODE) {
            if (AppConstants.Development.Http.HTTP_REQUEST_DELAY_TIME > 0) {
                clntBldr.addNetworkInterceptor(new Interceptor() {
                    @Override
                    public okhttp3.Response intercept(Chain chain) throws IOException {
                        SystemClock.sleep(AppConstants.Development.Http.HTTP_REQUEST_DELAY_TIME);
                        return chain.proceed(chain.request());
                    }
                });
            }

            if (AppConstants.Development.Http.LOG_HTTP_REQUESTS) {
                // Note: Logging must be added as the last interceptor so information provided with previous
                // interceptors can be logged.
                clntBldr.addInterceptor(new HttpLoggingInterceptor().setLevel(
                        HttpLoggingInterceptor.Level.HEADERS));
            }
        }

        ENDPOINTS_API = new Retrofit.Builder()
                .baseUrl(AppConstants.SERVER_BASE_URL)
                .client(clntBldr.build())
                .addConverterFactory(JacksonConverterFactory.create(new ObjectMapper().configure(
                        DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)))
                .build().create(EndpointsApi.class);
    }

    public static EndpointsCaller getInstance() {
        if (EndpointsCaller == null) {
            EndpointsCaller = new EndpointsCaller();
        }
        return EndpointsCaller;
    }

    public enum Endpoint {
        GET_HIGH_SCHOOLS,
        GET_SAT_SCORES,
    }

    public void makeGetHighSchoolsRequest(OnRequestsFinishedListener onRequestFinishedListener) {
        sCurrentCallRequestEndpoint = GET_HIGH_SCHOOLS;
        sCurrentCall = ENDPOINTS_API.getSchoolListRequest();
        sCurrentCall.enqueue(new RequestCallback(sCurrentCallRequestEndpoint, onRequestFinishedListener));
    }

    public void makeGetSatScoresRequest(OnRequestsFinishedListener onRequestFinishedListener) {
        sCurrentCallRequestEndpoint = GET_SAT_SCORES;
        sCurrentCall = ENDPOINTS_API.getSatScoresRequest();
        sCurrentCall.enqueue(new RequestCallback(sCurrentCallRequestEndpoint, onRequestFinishedListener));
    }

    /**
     * The request callback
     */
    private class RequestCallback implements Callback<okhttp3.ResponseBody> {
        private Endpoint mEndpointsRequestType;
        private OnRequestsFinishedListener mOnRequestFinishedListener;

        private RequestCallback(Endpoint endpointRequestType, OnRequestsFinishedListener onRequestFinishedListener) {
            mEndpointsRequestType = endpointRequestType;
            mOnRequestFinishedListener = onRequestFinishedListener;
        }

        @Override
        public void onResponse(Call<okhttp3.ResponseBody> call, Response<okhttp3.ResponseBody> response) {
            sCurrentCallRequestEndpoint = null;
            sCurrentCall = null;

            String reqsUrl = call.request().url().toString();
            LOGGER.info("{}: {} ({})", reqsUrl, response.code(), response.message());

            Object respBodyTknzJsonObje = null;
            try (okhttp3.ResponseBody okHttpRespBody = response.body()) {
                String respBodyStrn = okHttpRespBody.string();
                JSONObject respBodyJsonObje = null;
                JSONArray respBodyJsonArray = null;
                if (!respBodyStrn.equals("")) {
                    respBodyTknzJsonObje = new JSONTokener(respBodyStrn).nextValue();
                    if (respBodyTknzJsonObje instanceof JSONObject) {
                        respBodyJsonObje = (JSONObject) respBodyTknzJsonObje;
                    } else if (respBodyTknzJsonObje instanceof JSONArray) {
                        respBodyJsonArray = (JSONArray) respBodyTknzJsonObje;
                    } else {
                        throw new IllegalArgumentException(
                                "The response body is neither a JSON object nor an array; Response body: \"" +
                                        respBodyStrn + "\"");
                    }

                    if (AppConstants.ENABLE_DEVELOPMENT_MODE && AppConstants.Development.Http.LOG_HTTP_REQUESTS) {
                        LOGGER.debug("Response body " + reqsUrl + ": \n{}", ((respBodyJsonObje != null) ?
                                respBodyJsonObje.toString(4) : respBodyJsonArray.toString(4)));
                    }
                }

                switch (mEndpointsRequestType) {
                    case GET_HIGH_SCHOOLS:
                        HTTP_REQUEST_METADATA.get().edit().putLong(
                                HTTP_REQUEST_METADATA.getHighSchools_responseReceivedElapsedRealtime,
                                SystemClock.elapsedRealtime()).apply();
                        HTTP_REQUEST_DATA.get().edit().putString(
                                HTTP_REQUEST_DATA.getHighSchools_json, respBodyJsonArray.toString()).apply();
                        break;
                    case GET_SAT_SCORES:
                        HTTP_REQUEST_METADATA.get().edit().putLong(
                                HTTP_REQUEST_METADATA.getSatScores_responseReceivedElapsedRealtime,
                                SystemClock.elapsedRealtime()).apply();
                        HTTP_REQUEST_DATA.get().edit().putString(
                                HTTP_REQUEST_DATA.getSatScores_json, respBodyJsonArray.toString()).apply();


                        break;
                }

                if (mOnRequestFinishedListener != null) {
                    mOnRequestFinishedListener.onRequestFinished(null);
                }
            } catch (NullPointerException e) {
                HttpRequestError reqsError;
                reqsError = new HttpRequestError(FAILED_TO_GET_RESPONSE_BODY, e.toString());
                postResponseEventContainingRequestError(mEndpointsRequestType, reqsError, mOnRequestFinishedListener);
                LOGGER.error("{}: {}", reqsUrl, reqsError.getLogMessage());
            } catch (IOException | JSONException e) {
                HttpRequestError reqsError;
                if (e instanceof IOException) {
                    reqsError = new HttpRequestError(FAILED_TO_GET_RESPONSE_BODY, e.toString());
                } else {
                    reqsError = new HttpRequestError(RESPONSE_BODY_PROCESSING_FAILED, e.toString(),
                            (respBodyTknzJsonObje != null) ? respBodyTknzJsonObje.toString() : null);
                }

                postResponseEventContainingRequestError(mEndpointsRequestType, reqsError, mOnRequestFinishedListener);

                LOGGER.error("{}: {}", reqsUrl, reqsError.getLogMessage());
            }
        }

        @Override
        public void onFailure(Call<okhttp3.ResponseBody> call, Throwable throwable) {
            sCurrentCallRequestEndpoint = null;
            sCurrentCall = null;

            String reqsUrl = call.request().url().toString();
            if (call.isCanceled()) {
                LOGGER.info("Request canceled for {}", reqsUrl);
            } else {
                LOGGER.error("Request failed for {}: {}", reqsUrl, throwable.toString());
            }
        }
    }

    /**
     * Post a response event containing either a request or endpoints request error (contained in a CommonResponseBody).
     * @param endpointsRequestType The type of the endpoints request made
     * @param requestError The request error
     */
    private void postResponseEventContainingRequestError(
            Endpoint endpointsRequestType, HttpRequestError requestError,
            OnRequestsFinishedListener onRequestsFinishedListener) {
        switch (endpointsRequestType) {
            case GET_HIGH_SCHOOLS:
            case GET_SAT_SCORES:
                if (onRequestsFinishedListener != null) {
                    onRequestsFinishedListener.onRequestFinished(requestError);
                }
                break;
            default:
                if (onRequestsFinishedListener != null) {
                    onRequestsFinishedListener.onRequestFinished(requestError);
                }
                break;
        }
    }

    /**
     * Get the current call.
     */
    public Call<okhttp3.ResponseBody> getCurrentCall() {
        return sCurrentCall;
    }

    /**
     * Get the current call request type.
     */
    public Endpoint getCurrentCallRequestType() {
        return sCurrentCallRequestEndpoint;
    }

    /**
     * The get session and base data request callback
     */
    public interface OnRequestsFinishedListener {
        void onRequestFinished(BaseHttpRequestError baseHttpRequestError);
    }

}
