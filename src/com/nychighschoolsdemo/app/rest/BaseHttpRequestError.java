package com.nychighschoolsdemo.app.rest;

/**
 * The base request error
 */
public interface BaseHttpRequestError {

    interface BASE_TYPE { }

    BASE_TYPE getType();
    String getMessage();

}
