package com.nychighschoolsdemo.app.rest;

import com.nychighschoolsdemo.app.base.AppSharedPreferences.HTTP_REQUEST_DATA;
import com.nychighschoolsdemo.app.model.HighSchool;
import com.nychighschoolsdemo.app.model.SatScores;
import com.nychighschoolsdemo.app.util.SerializeUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * The endpoints data
 */
public class EndpointsData {
    private static final Logger LOGGER = LoggerFactory.getLogger(EndpointsData.class);
    private static List<HighSchool> sHighSchoolList;

    /**
     * Get the high school list
     */
    public static List<HighSchool> getHighSchoolList() {
        if (sHighSchoolList == null) {
            String highSchlJson = HTTP_REQUEST_DATA.get().getString(HTTP_REQUEST_DATA.getHighSchools_json, null);
            String satScoresJson = HTTP_REQUEST_DATA.get().getString(HTTP_REQUEST_DATA.getSatScores_json, null);
            if (highSchlJson != null && satScoresJson != null) {
                try {
                    JSONArray highSchoolsJsonArray = new JSONArray(highSchlJson);
                    JSONArray satScoresJsonArray = new JSONArray(satScoresJson);
                    sHighSchoolList = new ArrayList<>();
                    for (int i = 0; i < highSchoolsJsonArray.length(); i++) {
                        JSONObject highSchlJsonObje = highSchoolsJsonArray.getJSONObject(i);
                        HighSchool highSchl = SerializeUtils.deserialize(highSchlJsonObje, HighSchool.class);
                        for (int j = 0; j < satScoresJsonArray.length(); j++) {
                            JSONObject satScoreJsonObje = satScoresJsonArray.getJSONObject(j);
                            if (satScoreJsonObje.getString("dbn").equals(
                                    highSchlJsonObje.getString("dbn"))) {
                                SatScores satScores = SerializeUtils.deserialize(satScoreJsonObje, SatScores.class);
                                highSchl.setSatScores(satScores);
                                break;
                            }
                        }
                        sHighSchoolList.add(highSchl);
                    }
                } catch (IOException | JSONException e) {
                    LOGGER.error("Failed to deserialize JSON to HighSchool: {}", e.toString());
                }
            }
        }
        return sHighSchoolList;
    }
    
}
