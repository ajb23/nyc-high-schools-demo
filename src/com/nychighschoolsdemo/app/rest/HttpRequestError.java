package com.nychighschoolsdemo.app.rest;

/**
 * A request error
 */
public class HttpRequestError implements BaseHttpRequestError {

    public enum TYPE implements BASE_TYPE {
        /*
        General errors
         */
        FAILED_TO_CONNECT_TO_SERVER("Failed to connect to server.", null),

        /*
        Errors resulting from client or server code or logic
         */
        RESPONSE_CODE_NOT_200("%s (%s)", "A server error occurred."),
        RESPONSE_BODY_PROCESSING_FAILED("Failed to process response body: %s\nResponse body: \"%s\"",
                "An error occurred."),

        /*
        Errors resulting from network problems
         */
        FAILED_TO_GET_RESPONSE_BODY("Failed to get response body: %s", "An error occurred.");

        private String mLogMessage;
        private String mUserMessage;

        TYPE(String logMessage, String userMessage) {
            mLogMessage = logMessage;
            mUserMessage = (userMessage == null) ? logMessage : userMessage;
        }

        public String getLogMessage() {
            return mLogMessage;
        }

        public String getUserMessage() {
            return mUserMessage;
        }
    }

    private TYPE mType;
    private String mLogMessage;
    private String mUserMessage;

    public HttpRequestError(TYPE type, Object... messageArguments) {
        mType = type;
        if (messageArguments.length == 0) {
            mLogMessage = type.getLogMessage();
            mUserMessage = type.getUserMessage();
        } else {
            mLogMessage = String.format(type.getLogMessage(), messageArguments);
            mUserMessage = String.format(type.getUserMessage(), messageArguments);
        }
    }

    @Override
    public TYPE getType() {
        return mType;
    }

    @Override
    public String getMessage() {
        return mUserMessage;
    }

    public String getLogMessage() {
        return mLogMessage;
    }

}
