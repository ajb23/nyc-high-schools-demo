package com.nychighschoolsdemo.app.base;

import android.app.Activity;
import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.util.Log;
import com.google.android.gms.common.api.GoogleApiClient;
import com.nychighschoolsdemo.app.base.AppSharedPreferences.HTTP_REQUEST_METADATA;
import com.nychighschoolsdemo.app.util.FileUtils;
import com.nychighschoolsdemo.app.util.LogUtils;

import java.io.File;

/**
 * The application class
 */
/*
  BUCK EXOPACKAGE:
    Extend "DefaultApplicationLike" if using Buck Exopackage and "android.app.Application" if not.
 */
public final class Application extends android.app.Application {
    /*
      Note:
        The following two variables must be static to make the Application class work with Buck Exopackage even if
        context objects should not be placed in static fields.
     */
    private static Application sInstance;
    private static android.app.Application sContext;
    //private Service mService;
    private Activity mCurrentForegroundActivity;

    private GoogleApiClient mGoogleApiClient;

    //private boolean mIsServiceBound;

    /*
      BUCK EXOPACKAGE:
        Comment the following constructor if not using Buck Exopackage and comment if not (as subclasses of
        android.app.Application require a default constructor).
     */
    //public Application(android.app.Application context) {
    //    sContext = context;
    //}

    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;
        /*
          BUCK EXOPACKAGE:
            Comment the following line if using Buck Exopackage.
         */
        sContext = this;

        // If the app was force killed or sometimes even terminated gracefully, the Shared Preferences data will remain.
        // So we have to delete it.
        HTTP_REQUEST_METADATA.clear();

        if (AppConstants.ENABLE_DEVELOPMENT_MODE) {
            String filePath = FileUtils.AndroidUtils.getExternalStorageDirectory() + "/logcat.txt";
            File logFile = new File(filePath);
            if (logFile.exists() && logFile.length() / 1000 > 50) {
                logFile.delete();
            }
            LogUtils.writeLogsToFile(FileUtils.AndroidUtils.getExternalStorageDirectory() + "/logcat.txt");
        }
    }

    @Override
    public void onTerminate() {
        sContext.unbindService(mServiceConnection);

        //mService = null;
        super.onTerminate();
    }

    private ServiceConnection mServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName className, IBinder binder) {
            Log.i("Application", "Service started");
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            // Note: This method will not be called after calling unbindService() when using Context.BIND_AUTO_CREATE
            //mIsServiceBound = false;
            Log.i("Application", "Service disconnected");
        }
    };

    public static Application getInstance() {
        return sInstance;
    }

    public static android.app.Application getContext() {
        return sContext;
    }

    public Activity getCurrentForegroundActivity() {
        return mCurrentForegroundActivity;
    }

    public void setCurrentForegroundActivity(Activity currentForegroundActivity) {
        mCurrentForegroundActivity = currentForegroundActivity;
    }

    public GoogleApiClient getGoogleApiClient() {
        return mGoogleApiClient;
    }

    //public boolean isServiceBound() {
        // Note: The service object will not be nulled after calling unbindService
        //   since calling unbindService does not mean onServiceDisconnected will be called.
    //    return mIsServiceBound;
    //}

}
