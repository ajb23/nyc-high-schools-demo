package com.nychighschoolsdemo.app.base;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * The application shared preferences
 */
public class AppSharedPreferences {

    public static class HTTP_REQUEST_DATA {
        public static final String getHighSchools_json = "httpRequestData_getHighSchools_json";
        public static final String getSatScores_json = "httpRequestData_getSatScores_json";
        public static android.content.SharedPreferences get() {
            return Application.getContext().getSharedPreferences("httpRequestData", Context.MODE_PRIVATE);
        }
    }

    public static class HTTP_REQUEST_METADATA {
        public static final String getHighSchools_responseReceivedElapsedRealtime =
                "httpRequestMetadata_getHighSchools_responseReceivedElapsedRealtime";
        public static final String getSatScores_responseReceivedElapsedRealtime =
                "httpRequestMetadata_getSatScores_responseReceivedElapsedRealtime";
        public static android.content.SharedPreferences get() {
            return Application.getContext().getSharedPreferences("httpRequestMetadata", Context.MODE_PRIVATE);
        }

        /**
         * Clear the Suite HTTP request metadata.
         */
        public static void clear() {
            SharedPreferences.Editor edtr = get().edit();
            edtr.remove(getHighSchools_responseReceivedElapsedRealtime);
            edtr.remove(getSatScores_responseReceivedElapsedRealtime);
            edtr.apply();
        }
    }

    public static class APP_STATE {
        public static final String lifecycle_hasHomeActivityBeenDestroyed =
                "appState_lifecycle_hasHomeActivityBeenDestroyed";
        public static android.content.SharedPreferences get() {
            return Application.getContext().getSharedPreferences("appState", Context.MODE_PRIVATE);
        }
    }

}
