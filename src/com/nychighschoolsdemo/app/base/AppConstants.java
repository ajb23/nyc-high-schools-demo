package com.nychighschoolsdemo.app.base;

/**
 * Application constants
 */
public class AppConstants {
    public static final boolean ENABLE_DEVELOPMENT_MODE = true;
    /**
     * The server base URL
     */
    public static final String SERVER_BASE_URL = "https://data.cityofnewyork.us/resource/";

    /**
     * Constants for development mode
     * <p></p>
     * These constants are disabled if {@code ENABLE_DEV_MODE} is false.
     */
    public static final class Development {

        public static final class Http {
            public static final boolean LOG_HTTP_REQUESTS = false;
            public static final int HTTP_REQUEST_DELAY_TIME = 0;
        }

    }

    public static final class Http {

        public static final int READ_TIMEOUT = (ENABLE_DEVELOPMENT_MODE) ? 4000 : 10000;
        public static final int CONNECTION_TIMEOUT = (ENABLE_DEVELOPMENT_MODE) ? 2000 : 9000;

    }

    public static final class Features {

        /**
         * Whether or not to allow voting on Suites a user is outside of.
         */
        public static final boolean ENABLE_VOTING_FOR_OUTSIDE_SUITES = true;
        public static final boolean LOAD_SUITES_AROUND_USER_LOCATION = false;

    }

    public static final class Files {

        public static final String SUITE_VIDEOS_DIRECTORY = Application.getContext().getFilesDir() + "/Suite-Videos";

    }

}
