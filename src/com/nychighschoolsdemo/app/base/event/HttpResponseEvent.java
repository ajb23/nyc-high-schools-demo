package com.nychighschoolsdemo.app.base.event;

import com.nychighschoolsdemo.app.rest.BaseHttpRequestError;
import com.nychighschoolsdemo.app.rest.EndpointsCaller;

/**
 * An HTTP response event
 */
public class HttpResponseEvent {
    private BaseHttpRequestError mBaseRequestError;
    private EndpointsCaller.Endpoint mEndpointsRequestType;
    private Object mResponseBodyJson;

    /**
     * Construct a response event containing a response body.
     * @param endpointsRequestType The type of the endpoints request made
     */
    public HttpResponseEvent(EndpointsCaller.Endpoint endpointsRequestType) {
        mEndpointsRequestType = endpointsRequestType;
    }

    /**
     * Construct a response event containing a request error.
     * @param endpointsRequestType The type of the endpoints request made
     * @param baseRequestError The base request error
     */
    public HttpResponseEvent(EndpointsCaller.Endpoint endpointsRequestType,
                                BaseHttpRequestError baseRequestError) {
        mEndpointsRequestType = endpointsRequestType;
        mBaseRequestError = baseRequestError;
    }

    /**
     * Construct a response event containing a response body JSON.
     * @param endpointsRequestType The type of the endpoints request made
     * @param responseBodyJson The response body JSON object or array
     */
    public HttpResponseEvent(EndpointsCaller.Endpoint endpointsRequestType,
                                Object responseBodyJson) {
        mEndpointsRequestType = endpointsRequestType;
        mResponseBodyJson = responseBodyJson;
    }

    public BaseHttpRequestError getBaseRequestError() {
        return mBaseRequestError;
    }

    public EndpointsCaller.Endpoint getEndpointsRequestType() {
        return mEndpointsRequestType;
    }

    public Object getResponseBodyJson() {
        return mResponseBodyJson;
    }

}
