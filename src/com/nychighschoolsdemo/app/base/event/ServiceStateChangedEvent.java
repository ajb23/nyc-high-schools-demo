package com.nychighschoolsdemo.app.base.event;

public class ServiceStateChangedEvent {

	private String componentConnected;
	
	public ServiceStateChangedEvent(String comConnected) {
		componentConnected = comConnected;
	}

	public String getComponentConnected() {
		return componentConnected;
	}
	
}
