package com.nychighschoolsdemo.app.ui;

import android.annotation.SuppressLint;

/**
 * The base activity
 * <p><p>
 * This activity contains application-specific base activity code.
 */
@SuppressLint("Registered")
public class BaseActivity extends GenericBaseActivity {

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

}
