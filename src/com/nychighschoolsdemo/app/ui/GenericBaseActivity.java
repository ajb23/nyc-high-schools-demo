package com.nychighschoolsdemo.app.ui;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ListView;
import com.nychighschoolsdemo.app.R;
import com.nychighschoolsdemo.app.base.Application;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;

/**
 * The generic base activity
 * <p><p>
 * This activity contains non-application-specific base activity code. It is not meant to replace any features provided
 * by the Android API (e.g. setting the content view) and instead provide new common generic ones (e.g. swapping the
 * current fragment).
 */
@SuppressLint("Registered")
public class GenericBaseActivity extends AppCompatActivity {
    private static final Logger LIFECYCLE_LOG = LoggerFactory.getLogger("ActivityLifecycle");
    private String mSubclassActivityName;
    protected int mLayoutId;
    protected int mActivityLayoutId;
    private Fragment mFragment;
    private int mMenuResId;
    public Menu mMenu;
    private String[] mDrawerItems;
    private AdapterView.OnItemClickListener mDrawerItemOnClickListener;

    /**
     * Whether or not this activity is visible
     */
    private boolean mIsInForeground;

    /**
     * Whether or not an activity's instance state was saved.
     * An activity's instance state is saved primary occurs primarily after one has stopped
     * This is needed to show UI components (e.g. a dialog) correctly after an activity is returns from a saved state
     * (happens, e.g., when it is returning from the Settings or a browser application).
     */
    private boolean mWasInstanceStateSaved;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LIFECYCLE_LOG.trace("{}: onCreate()", mSubclassActivityName);

        // Note: The base layout ID must be checked if equals 0 because it may be assigned by a subclassing activity.
        if (mLayoutId == 0) {
            if (mDrawerItems == null) {
                mLayoutId = R.layout.app_o_generic_base_activity;
            } else {
                mLayoutId = R.layout.app_o_generic_base_activity_w_drawer;
            }

            setContentView(mLayoutId);

            if (mDrawerItems != null) {
                ListView listView = (ListView) findViewById(R.id.lv_drawer);
                listView.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, Arrays.asList(
                        mDrawerItems)));
                listView.setOnItemClickListener(mDrawerItemOnClickListener);
            }
        } else {
            setContentView(mLayoutId);
        }

        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));

        if (mActivityLayoutId != 0) {
            getLayoutInflater().inflate(mActivityLayoutId, getGenericBaseActivityContentFrameLayout());
        } else if (mFragment != null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fl_activity_content_container, mFragment, mFragment.getClass().getSimpleName())
                    .commit();
        }

        getWindow().setSoftInputMode(
                // Keep the action bar from moving up when the soft keyboard is shown.
                WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE
                // Do not leave the soft keyboard open when entering an Activity and a view requiring it is focused.
                | WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    @Override
    public View onCreateView(String name, Context context, AttributeSet attrs) {
        LIFECYCLE_LOG.trace("{}: onCreateView()", mSubclassActivityName);
        return super.onCreateView(name, context, attrs);
    }

    @Override
    protected void onStart() {
        super.onStart();
        LIFECYCLE_LOG.trace("{}: onStart()", mSubclassActivityName);
    }

    @Override
    protected void onResume() {
        Application.getInstance().setCurrentForegroundActivity(this);
        mIsInForeground = true;

        super.onResume();
        LIFECYCLE_LOG.trace("{}: onResume()", mSubclassActivityName);
    }

    @Override
    protected void onPause() {
        LIFECYCLE_LOG.trace("{}: onPause()", mSubclassActivityName);

        if (this.equals(Application.getInstance().getCurrentForegroundActivity())) {
            Application.getInstance().setCurrentForegroundActivity(null);
        }
        mIsInForeground = false;
        super.onPause();
    }

    @Override
    protected void onStop() {
        LIFECYCLE_LOG.trace("{}: onStop()", mSubclassActivityName);
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        LIFECYCLE_LOG.trace("{}: onDestroy()", mSubclassActivityName);
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (mMenuResId != -1) {
            getMenuInflater().inflate(mMenuResId, menu);
        }
        mMenu = menu;

        LIFECYCLE_LOG.trace("{}: Activity onCreateOptionsMenu()", mSubclassActivityName);

        return true;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        mWasInstanceStateSaved = true;

        super.onSaveInstanceState(outState);
    }

    /**
     * Get the root {@link ViewGroup} of the generic base activity. This is the root ViewGroup of the layout passed to
     * #setContentView(int).
     */
    private ViewGroup getGenericBaseActivityRootViewGroup() {
        ViewGroup andrSystAppRootViewGroup = (ViewGroup) findViewById(android.R.id.content);
        if (andrSystAppRootViewGroup == null) {
            andrSystAppRootViewGroup = (ViewGroup) getWindow().getDecorView().findViewById(android.R.id.content);
        }
        return (ViewGroup) andrSystAppRootViewGroup.getChildAt(0);
    }

    /**
     * Get the generic base activity-activity content container {@link FrameLayout}.
     */
    private FrameLayout getGenericBaseActivityContentFrameLayout() {
        // Note: The activity content container FrameLayout must be accessed via #findViewById(int) rather than by child
        // index because GenericBaseActivity has two layouts: one with a drawer and one without a drawer and if the
        // former is used, the child indexes will not be the same.
        return (FrameLayout) getGenericBaseActivityRootViewGroup().findViewById(R.id.fl_activity_content_container);
    }

    /*
        Activity initialization methods

        One of the following methods is called by each subclassing Activity.
     */

    public void initActivity(Class activityClass, int layoutId) {
        initGenricBaseActivityHelper(activityClass, -1, layoutId, null);
    }

    public void initActivity(Class activityClass, int layoutId, int menuResId) {
        initGenricBaseActivityHelper(activityClass, menuResId, layoutId, null);
    }

    public void initActivity(Class activityClass, Fragment fragment) {
        initGenricBaseActivityHelper(activityClass, -1, 0, fragment);
    }

    public void initActivity(Class activityClass, Fragment fragment, int menuResId) {
        initGenricBaseActivityHelper(activityClass, menuResId, 0, fragment);
    }

    private void initGenricBaseActivityHelper(Class activityClass, int menuResId, int layoutId, Fragment fragment) {
        mSubclassActivityName = activityClass.getSimpleName();
        mMenuResId = menuResId;
        mActivityLayoutId = layoutId;
        mFragment = fragment;
    }

    /*
        Layout control methods
     */

    /**
     * Replace the current content with a new fragment.
     */
    public void addNextFragment(Fragment fragment) {
        android.support.v4.app.FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
        ft.replace(R.id.fl_activity_content_container, fragment, fragment.getClass().getSimpleName())
                .addToBackStack(null)
                .commit();
    }

    /**
     * Enable a drawer with this activity and give it items.
     * @param drawerItems The items of the drawer
     */
    public void enableDrawer(String[] drawerItems, AdapterView.OnItemClickListener onItemClickListener) {
        mDrawerItems = drawerItems;
        mDrawerItemOnClickListener = onItemClickListener;
    }

    /*
        State information methods
     */

    /**
     * Check whether or not the activity is in the foreground.
     */
    public boolean isInForeground() {
        return mIsInForeground;
    }

    public boolean wasInstanceStateSaved() {
        return mWasInstanceStateSaved;
    }

}
