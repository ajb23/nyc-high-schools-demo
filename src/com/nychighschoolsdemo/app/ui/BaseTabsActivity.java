package com.nychighschoolsdemo.app.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import com.nychighschoolsdemo.app.R;
import com.nychighschoolsdemo.app.ui.global.TouchInterceptSupportViewPager;
import com.nychighschoolsdemo.app.ui.global.widget.SlidingTabLayout;

public class BaseTabsActivity extends BaseActivity {
    private static final STYLE TABS_STYLE = STYLE.NON_SLIDING;
    private static final THEME TABS_THEME = THEME.GREY;
    public SlidingTabLayout mSlidingTabLayout;
    public TouchInterceptSupportViewPager mViewPager;
    public MFragmentPagerAdapter mMFragmentPagerAdapter;
    protected Fragment[] mViewPagerFragments;
    private String[] mViewPagerFragmentPageTitles;

    private enum STYLE {
        NON_SLIDING, SLIDING
    }

    private enum THEME {
        GREY, LIGHT
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        if (mViewPagerFragments != null) {
            if (TABS_STYLE == STYLE.NON_SLIDING) {
                if (TABS_THEME == THEME.GREY) {
                    mLayoutId = R.layout.app_s_generic_base_tabs_activity_o_non_sliding_layout_grey;
                } else {
                    mLayoutId = R.layout.app_s_generic_base_tabs_activity_o_non_sliding_layout_light;
                }
            }
        }
        super.onCreate(savedInstanceState);

        if (mViewPagerFragments != null) {
            mViewPager = (TouchInterceptSupportViewPager) findViewById(R.id.view_pager);
            mMFragmentPagerAdapter = new MFragmentPagerAdapter();
            mViewPager.setAdapter(mMFragmentPagerAdapter);

            if (TABS_STYLE == STYLE.NON_SLIDING) {
                mSlidingTabLayout = (SlidingTabLayout) findViewById(R.id.sliding_tab_layout);
                if (TABS_THEME == THEME.GREY) {
                    mSlidingTabLayout.setSelectedIndicatorColors(ContextCompat.getColor(this,
                            R.color.app_tabs_bar_indicator_grey));
                    mSlidingTabLayout.setTextColor(ContextCompat.getColor(this, R.color.app_tabs_bar_text_grey));
                } else {
                    mSlidingTabLayout.setSelectedIndicatorColors(ContextCompat.getColor(this,
                            R.color.app_tabs_bar_indicator_light));
                    mSlidingTabLayout.setTextColor(ContextCompat.getColor(this, R.color.app_tabs_bar_text_light));
                }
                mSlidingTabLayout.setDistributeEvenly(true);
                mSlidingTabLayout.setViewPager(mViewPager);
            }
        }
    }

    public void initActivity(Class mclass, Fragment[] fragments, String[] fragmentPageTitles) {
        initActivity(mclass, fragments, fragmentPageTitles, -1);
    }

    /**
     * Must be called by each subclassing activity in its onCreate right before the call to super
     */
    public void initActivity(Class mclass, Fragment[] fragments, String[] fragmentPageTitles, int menuResId) {
        super.initActivity(mclass, 0, menuResId);

        if (fragments.length != fragmentPageTitles.length) {
            throw new IllegalStateException("Argument fragments and fragment pages titles array lengths do not match");
        }

        mViewPagerFragments = fragments;
        mViewPagerFragmentPageTitles = fragmentPageTitles;
    }

    public TouchInterceptSupportViewPager getInterceptSupportViewPager() {
        return mViewPager;
    }

    public void setViewPagerFragments(Fragment[] fragments) {
        mViewPagerFragments = fragments;
    }

    public void setViewPagerFragmentsPageTitles(String[] pageTitles) {
        mViewPagerFragmentPageTitles = pageTitles;
    }

    public MFragmentPagerAdapter getFragmentPagerAdapter() {
        return mMFragmentPagerAdapter;
    }

    /**
     * View pager adapter allowing swappable Fragments. This is done simply by
     * having a Fragment implement the SwapFragment interface.
     */
    public class MFragmentPagerAdapter extends FragmentPagerAdapter {
        private Fragment[] mViewPagerPreviousFragments = new Fragment[mViewPagerFragments.length];

        public MFragmentPagerAdapter() {
            super(getSupportFragmentManager());
        }

        @Override
        public void notifyDataSetChanged() {
            super.notifyDataSetChanged();
            mSlidingTabLayout.notifyDataSetChanged();
        }

        /**
         * Used by tabs activities to swap fragments inside the viewpager.
         */
        public void swapFragment(Fragment currentFragment, Fragment newFragment, String pageTitle) {
            int currFrgmViewPagrPost = -1;

            for (int i = 0; i < mViewPagerFragments.length; i++) {
                if (mViewPagerFragments[i].getClass().isInstance(currentFragment)) {
                    currFrgmViewPagrPost = i;
                    break;
                }
            }

            if (currFrgmViewPagrPost == -1) {
                throw new IllegalArgumentException("Cannot swap fragment: Fragment \"" +
                        currentFragment.getClass().getSimpleName() + "\" instance not found in this activity");
            }

            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.remove(mViewPagerFragments[currFrgmViewPagrPost]);
            if (wasInstanceStateSaved()) {
                ft.commitAllowingStateLoss();
            } else {
                ft.commit();
            }

            mViewPagerPreviousFragments[currFrgmViewPagrPost] = mViewPagerFragments[currFrgmViewPagrPost];

            mViewPagerFragments[currFrgmViewPagrPost] = newFragment;
            mViewPagerFragmentPageTitles[currFrgmViewPagrPost] = pageTitle;

            notifyDataSetChanged();
        }

        @Override
        public Fragment getItem(int position) {
            return mViewPagerFragments[position];
        }

        @Override
        public int getCount() {
            return mViewPagerFragments.length;
        }

        /**
         * Gets the position of the current item, before it is changed.
         */
        @Override
        public int getItemPosition(Object object) {
            // POSITION_NONE required for swapped fragments
            for (int i = 0; i < mViewPagerFragments.length; i++) {
                if (object.getClass().isInstance(mViewPagerPreviousFragments[i])) {
                    if (mViewPagerFragments[i] != mViewPagerPreviousFragments[i]) {
                        return POSITION_NONE;
                    }
                }
            }
            return POSITION_UNCHANGED;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mViewPagerFragmentPageTitles[position];
        }
    }

}
