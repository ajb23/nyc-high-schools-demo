package com.nychighschoolsdemo.app.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BaseFragment extends Fragment {
    private static final Logger LIFECYCLE_LOG = LoggerFactory.getLogger("FragmentLifecycle");
    private String mSubclassingFragmentName;
    protected Bundle mRefreshLayoutArgs;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LIFECYCLE_LOG.trace("{}: onCreate()", mSubclassingFragmentName);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        LIFECYCLE_LOG.trace("{}: onCreateView()", mSubclassingFragmentName);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        LIFECYCLE_LOG.trace("{}: onViewCreated()", mSubclassingFragmentName);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        LIFECYCLE_LOG.trace("{}: onActivityCreated()", mSubclassingFragmentName);
    }

    @Override
    public void onStart() {
        super.onStart();
        LIFECYCLE_LOG.trace("{}: onStart()", mSubclassingFragmentName);
    }

    @Override
    public void onStop() {
        LIFECYCLE_LOG.trace("{}: onStop()", mSubclassingFragmentName);
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        LIFECYCLE_LOG.trace("{}: onDestroyView()", mSubclassingFragmentName);
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        LIFECYCLE_LOG.trace("{}: onDestroy()", mSubclassingFragmentName);
        super.onDestroy();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        LIFECYCLE_LOG.trace("{}: onActivityResult()", mSubclassingFragmentName);
        super.onActivityResult(requestCode, resultCode, data);
    }

    /**
     * Must be called by each subclassing fragment in its constructor
     */
    public void initFragment(Class fragmentClass) {
        mSubclassingFragmentName = fragmentClass.getSimpleName();
    }

    public String getSubclassingFragmentName() {
        return mSubclassingFragmentName;
    }

}
