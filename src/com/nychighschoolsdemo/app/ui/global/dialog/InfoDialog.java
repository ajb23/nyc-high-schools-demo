package com.nychighschoolsdemo.app.ui.global.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.nychighschoolsdemo.app.R;
import com.nychighschoolsdemo.app.base.Application;
import com.nychighschoolsdemo.app.util.DisplayUtils;

/**
 * A generic dialog for showing information or getting choices
 * Tmp note: Copied from P#2
 */
public class InfoDialog extends DialogFragment implements BaseDialog {
    private Context mContext;
    private Bundle mBundleArguments;
    private OnDialogCreatedListener mOnDialogCreatedListener;
    private BaseOnClickListener mBaseOnClickListener;

    // TODO: Move some of these new instance method functions to separate functions (also swap negBttnText and postBttnText order)

    /**
     * Get a new instance for displaying a message.
     */
    public static InfoDialog newInstance(Context context, String message) {
        return newInstanceHelper(context, null, message, -1, null, null,
                null, null, null);
    }

    /**
     * Get a new instance for displaying a title and message.
     */
    public static InfoDialog newInstance(Context context, String title, String message) {
        return newInstanceHelper(context, title, message, -1, null, null,
                null, null, null);
    }

    /**
     * Get a new instance for displaying a title and list of string items.
     */
    public static InfoDialog newListItemsInstance(Context context, String title, String[] listItems) {
        return newInstanceHelper(context, title, null, -1, listItems, null,
                null, null, null);
    }

    /**
     * Get a new instance for displaying a title and specified layout.
     */
    public static InfoDialog newInstance(Context context, String title, int layoutId) {
        return newInstanceHelper(context, title, null, layoutId, null, null,
                null, null, null);
    }

    /**
     * Get a new instance for a message and setting the on dialog created listener.
     */
    public static InfoDialog newInstance(Context context, String message,
                                         OnDialogCreatedListener onDialogCreatedListener) {
        return newInstanceHelper(context, null, message, -1, null, onDialogCreatedListener,
                null, null, null);
    }

    /**
     * Get a new instance for displaying a title and message and setting the on dialog created listener.
     */
    public static InfoDialog newInstance(Context context, String title, String message,
                                         OnDialogCreatedListener onDialogCreatedListener) {
        return newInstanceHelper(context, title, message, -1, null, onDialogCreatedListener,
                null, null, null);
    }

    /**
     * Get a new instance for displaying a message and setting an OnClickListener.
     */
    public static InfoDialog newInstance(Context context, String message,
                                         BaseOnClickListener baseOnClickListener) {
        return newInstanceHelper(context, null, message, -1, null, null,
                baseOnClickListener, null, null);
    }

    /**
     * Get a new instance for displaying a title and message and setting an OnClickListener.
     */
    public static InfoDialog newInstance(Context context, String title, String message,
                                         BaseOnClickListener baseOnClickListener) {
        return newInstanceHelper(context, title, message, -1, null, null,
                baseOnClickListener, null, null);
    }

    /**
     * Get a new instance for displaying a title and list of string items and setting an OnClickListener.
     */
    public static InfoDialog newListItemsInstance(Context context, String title, String[] listItems,
                                                  BaseOnClickListener onClickListener) {
        return newInstanceHelper(context, title, null, -1, listItems, null,
                onClickListener, null, null);
    }

    /**
     * Get a new instance for displaying a title and specified layout and setting an OnClickListener.
     */
    public static InfoDialog newInstance(Context context, String title, int layoutId,
                                         BaseOnClickListener baseOnClickListener) {
        return newInstanceHelper(context, title, null, layoutId, null, null,
                baseOnClickListener,
                null, null);
    }

    /**
     * Get a new instance for displaying a message and setting an OnClickListener and the button texts.
     */
    public static InfoDialog newInstance(Context context, String message,
                                      PositiveAndNegativeButtonOnClickListener positiveAndNegativeButtonOnClickListener,
                                      String negativeButtonText, String positiveButtonText) {
        return newInstanceHelper(context, null, message, -1, null, null,
                positiveAndNegativeButtonOnClickListener, negativeButtonText, positiveButtonText);
    }

    /**
     * Get a new instance for displaying a title and message and setting an OnClickListener and the button texts.
     */
    public static InfoDialog newInstance(Context context, String title, String message,
                                      PositiveAndNegativeButtonOnClickListener positiveAndNegativeButtonOnClickListener,
                                      String negativeButtonText, String positiveButtonText) {
        return newInstanceHelper(context, title, message, -1, null, null,
                positiveAndNegativeButtonOnClickListener, negativeButtonText, positiveButtonText);
    }

    /**
     * Helper method for all new instance methods.
     */
    private static InfoDialog newInstanceHelper(Context context, String title, String message, int layoutId,
                                                String[] listItems, OnDialogCreatedListener onDialogCreatedListener,
                                                BaseOnClickListener baseOnClickListener, String negativeButtonText,
                                                String positiveButtonText) {
        InfoDialog infoDlg = new InfoDialog();
        infoDlg.mContext = context;
        infoDlg.mOnDialogCreatedListener = onDialogCreatedListener;
        infoDlg.mBaseOnClickListener = baseOnClickListener;

        Bundle args = new Bundle();
        args.putString("title", title);
        args.putString("message", message);
        args.putInt("layoutId", layoutId);
        args.putStringArray("listItems", listItems);
        args.putString("positiveButtonText", positiveButtonText);
        args.putString("negativeButtonText", negativeButtonText);
        infoDlg.setArguments(args);
        return infoDlg;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBundleArguments = getArguments();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext, R.style.AppAlertDialogStyle);
        String title = mBundleArguments.getString("title");
        String mesg = mBundleArguments.getString("message");
        int laytId = mBundleArguments.getInt("layoutId");
        String listItems[] = mBundleArguments.getStringArray("listItems");
        String negtButnText = mBundleArguments.getString("negativeButtonText");
        String postButnText = mBundleArguments.getString("positiveButtonText");
        boolean parseMesgAsHtml = mBundleArguments.getBoolean("parseMessageAsHtml");

        if (title != null) {
            builder.setTitle(title);
        }

        // TODO: Apply this cb to the list adapter also
        CheckBox doNotShowAgainCheckBox = null;
        if (mBaseOnClickListener instanceof PositiveButtonOnClickListenerWithDoNotShowAgainCheckBox ||
                mBaseOnClickListener instanceof PositiveAndNegativeButtonOnClickListenerWithDoNotShowAgainCheckBox) {
            doNotShowAgainCheckBox = (CheckBox) LayoutInflater.from(mContext).inflate(
                    R.layout.app_s_widgets_s_generic_o_checkbox, null);
            doNotShowAgainCheckBox.setText("Do not show again");
        }

        if (mesg != null) {
            if (doNotShowAgainCheckBox != null || parseMesgAsHtml) {
                TextView tvMesg = (TextView) LayoutInflater.from(mContext).inflate(
                        R.layout.app_s_generic_widgets_o_textview, null);
                // TODO: Need to update SDK version and uncomment the following (see line 94 in SectionsListBaseActivity)
                tvMesg.setText(Html.fromHtml(mesg));
                tvMesg.setMovementMethod(LinkMovementMethod.getInstance());
                tvMesg.setTextSize(16);

                View usedView;
                if (doNotShowAgainCheckBox != null) {
                    // Note: The LinearLayout must be in its own ScrollView rather than the one provided by the
                    // AlertDialog because if the height of the first view in the LinearLayout is greater than the max
                    // height of the AlertDialog, the second view in the LinearLayout will not show.
                    ScrollView sv = new ScrollView(mContext);
                    sv.setVerticalScrollBarEnabled(false);
                    LinearLayout ll = (LinearLayout) LayoutInflater.from(mContext).inflate(
                            R.layout.app_s_generic_widgets_o_linearlayout, null);
                    ll.addView(tvMesg);
                    ll.addView(doNotShowAgainCheckBox);
                    sv.addView(ll);
                    usedView = sv;

                    setDoNotShowAgainCheckBoxDefaultLayoutParameters(doNotShowAgainCheckBox);
                } else {
                    usedView = tvMesg;
                }

                setAlertDialogBuilderViewWithDefaultMargins(builder, usedView);
            } else {
                builder.setMessage(mesg);
            }
        } else if (laytId != -1) {
            View laytView = LayoutInflater.from(mContext).inflate(laytId, null);
            if (doNotShowAgainCheckBox != null) {
                ViewGroup ll = (ViewGroup) LayoutInflater.from(mContext).inflate(
                        R.layout.app_s_generic_widgets_o_linearlayout, null);
                ll.addView(laytView);
                ll.addView(doNotShowAgainCheckBox);
                setAlertDialogBuilderViewWithDefaultMargins(builder, ll);

                setDoNotShowAgainCheckBoxDefaultLayoutParameters(doNotShowAgainCheckBox);
            } else {
                // Do not set the the alert dialog to its default margins here because the margins are too large for
                // custom layouts.
                builder.setView(LayoutInflater.from(mContext).inflate(laytId, null));
            }
        } else if (listItems != null) {
            MListAdapter listAdapter = MListAdapter.newInstance(getActivity(), listItems);
            builder.setAdapter(listAdapter, null);
        }

        if (negtButnText == null) {
            negtButnText = "Cancel";
        }
        if (postButnText == null) {
            postButnText = "OK";
        }

        DialogInterface.OnClickListener onClickListener = null;
        final CheckBox finalCbDoNotShowAgain = doNotShowAgainCheckBox;
        if (mBaseOnClickListener != null) {
            if (mBaseOnClickListener instanceof PositiveButtonOnClickListener) {
                onClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ((PositiveButtonOnClickListener) mBaseOnClickListener).onClick(dialog);
                    }
                };
            } else if (mBaseOnClickListener instanceof PositiveAndNegativeButtonOnClickListener) {
                onClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ((PositiveAndNegativeButtonOnClickListener) mBaseOnClickListener).onClick(dialog, which);
                    }
                };
                builder.setNegativeButton(negtButnText, onClickListener);
            } else if (mBaseOnClickListener instanceof PositiveButtonOnClickListenerWithDoNotShowAgainCheckBox) {
                onClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ((PositiveButtonOnClickListenerWithDoNotShowAgainCheckBox) mBaseOnClickListener).onClick(dialog,
                                finalCbDoNotShowAgain.isChecked());
                    }
                };
            } else if (mBaseOnClickListener instanceof
                    PositiveAndNegativeButtonOnClickListenerWithDoNotShowAgainCheckBox) {
                onClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ((PositiveAndNegativeButtonOnClickListenerWithDoNotShowAgainCheckBox)
                                mBaseOnClickListener).onClick(dialog, which, finalCbDoNotShowAgain.isChecked());
                    }
                };
                builder.setNegativeButton(negtButnText, onClickListener);
            }
        }
        builder.setPositiveButton(postButnText, onClickListener);
        return builder.create();
    }

    @Override
    public void onStart() {
        super.onStart();
        if (mOnDialogCreatedListener != null) {
            mOnDialogCreatedListener.onDialogCreated(getDialog());
        }
    }

    @Override
    public void show() {
        if (!(mContext instanceof FragmentActivity)) {
            throw new IllegalArgumentException("The context is not a FragmentActivity.");
        }

        FragmentActivity f = (FragmentActivity) mContext;
        show(f.getSupportFragmentManager(), this.getClass().getSimpleName());
    }

    @Override
    public DialogFragment getFragment() {
        return this;
    }

    @Override
    public void setOnDialogCreatedListener(BaseDialog.OnDialogCreatedListener onDialogCreatedListener) {
        mOnDialogCreatedListener = onDialogCreatedListener;
    }

    /**
     * Set the view of an AlertDialog.Builder with default margins.
     */
    private void setAlertDialogBuilderViewWithDefaultMargins(AlertDialog.Builder bldr, View view) {
        float dens = Application.getContext().getResources().getDisplayMetrics().density;
        bldr.setView(view, (int) (24 * dens), (int) (18 * dens), (int) (24 * dens), 0);
    }

    /**
     * Set the default layout parameters of the Do not show again CheckBox.
     */
    private void setDoNotShowAgainCheckBoxDefaultLayoutParameters(CheckBox checkBox) {
        LinearLayout.LayoutParams laytParams = (LinearLayout.LayoutParams) checkBox.getLayoutParams();
        laytParams.topMargin = DisplayUtils.dpToPixels(22);
    }

    /**
     * Set the text of the positive button.
     */
    public void setPositiveButtonText(String text) {
        getArguments().putString("positiveButtonText", text);
    }

    /**
     * Set the text of the negative button.
     */
    public void setNegativeButtonText(String text) {
        getArguments().putString("negativeButtonText", text);
    }

    /**
     * Set whether or not to parse the message as HTML.
     */
    public void setParseMessageAsHtml(boolean value) {
        getArguments().putBoolean("parseMessageAsHtml", value);
    }


    private static class MListAdapter extends ArrayAdapter<String> {
        private LayoutInflater mLayoutInflater;
        private String[] mObjects;

        private MListAdapter(Activity activity, @LayoutRes int resource, @NonNull String[] objects) {
            super(activity, resource, objects);
            mLayoutInflater = activity.getLayoutInflater();
            mObjects = objects;
        }

        private static MListAdapter newInstance(Activity activity, @NonNull String[] objects) {
            return new MListAdapter(activity, R.layout.app_s_dialogs_s_info_dialog_s_list_s_adapter_o_row, objects);
        }

        private static class ViewHolder {
            public TextView tv;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                ViewHolder vHold = new ViewHolder();

                convertView = mLayoutInflater.inflate(R.layout.app_s_dialogs_s_info_dialog_s_list_s_adapter_o_row, parent,
                        false);
                vHold.tv = (TextView) convertView.findViewById(R.id.tv);
                convertView.setTag(vHold);
            }

            ViewHolder vHold = (ViewHolder) convertView.getTag();

            vHold.tv.setText(mObjects[position]);
            return convertView;
        }
    }

    /**
     * Base OnClickListener
     */
    public interface BaseOnClickListener { }

    /**
     * BaseOnClickListener for the positive button.
     */
    public interface PositiveButtonOnClickListener extends BaseOnClickListener {
        void onClick(DialogInterface dialog);
    }

    /**
     * BaseOnClickListener for the positive and negative buttons.
     */
    public interface PositiveAndNegativeButtonOnClickListener extends BaseOnClickListener {
        void onClick(DialogInterface dialog, int which);
    }

    /**
     * BaseOnClickListener for the positive button enabling the Do not show again checkbox.
     */
    public interface PositiveButtonOnClickListenerWithDoNotShowAgainCheckBox extends BaseOnClickListener {
        void onClick(DialogInterface dialog, boolean isDoNotShowAgainCheckBoxChecked);
    }

    /**
     * BaseOnClickListener for the positive and negative buttons enabling the Do not show again checkbox.
     */
    public interface PositiveAndNegativeButtonOnClickListenerWithDoNotShowAgainCheckBox extends BaseOnClickListener {
        void onClick(DialogInterface dialog, int which, boolean isDoNotShowAgainCheckBoxChecked);
    }

}
