package com.nychighschoolsdemo.app.ui.global.dialog;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewStub;
import android.widget.EditText;
import android.widget.TextView;
import com.nychighschoolsdemo.app.R;
import com.nychighschoolsdemo.app.util.UIUtils;

/**
 * A generic text input dialog for retrieving text input allowing up to two EditText fields
 */
public class TextInputDialog extends DialogFragment implements BaseDialog {
    private FragmentActivity mFragmentActivity;
    private Bundle mBundleArguments;
    private BaseOnClickListener mBaseOnClickListener;
    private OnDialogCreatedListener mOnDialogCreatedListener;

    private EditText mEt1;

    /**
     * Get a new instance for displaying a message.
     */
    public static TextInputDialog newInstance(FragmentActivity activity, String message,
                                              BaseOnClickListener baseOnClickListener) {
        return newInstanceHelper(activity, null, message, baseOnClickListener);
    }

    /**
     * Get a new instance for displaying a title and message.
     */
    public static TextInputDialog newInstance(FragmentActivity activity, String title, String message,
                                              BaseOnClickListener baseOnClickListener) {
        return newInstanceHelper(activity, title, message, baseOnClickListener);
    }

    /**
     * Helper method for all new instance methods.
     */
    private static TextInputDialog newInstanceHelper(FragmentActivity activity, String title, String message,
                                                     BaseOnClickListener baseOnClickListener) {
        final TextInputDialog textInputDilg = new TextInputDialog();
        textInputDilg.mFragmentActivity = activity;
        textInputDilg.mBaseOnClickListener = baseOnClickListener;

        Bundle args = new Bundle();
        args.putString("title", title);
        args.putString("message", message);
        textInputDilg.setArguments(args);
        return textInputDilg;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBundleArguments = getArguments();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder bldr = new AlertDialog.Builder(mFragmentActivity, R.style.AppAlertDialogStyle);

        bldr.setTitle(mBundleArguments.getString("title", null));
        bldr.setMessage(mBundleArguments.getString("message", null));

        View view = LayoutInflater.from(mFragmentActivity).inflate(R.layout.app_s_dialogs_o_text_input_dialog,
                null);

        bldr.setView(view);

        if (mBundleArguments.containsKey("firstEditTextFieldHeader")) {
            ((ViewStub) view.findViewById(R.id.vs_tv_1)).inflate();
            TextView tv = ((TextView) view.findViewById(R.id.tv_1));

            tv.setText(mBundleArguments.getString("firstEditTextFieldHeader"));
            setHeaderTextViewAttributes(tv);
        }

        mEt1 = (EditText) view.findViewById(R.id.et_1);
        EditText et2 = null;

        mEt1.setText(mBundleArguments.getString("firstEditTextFieldInitialText", null));

        if (mBaseOnClickListener instanceof TwoFieldsOnClickListener ||
                mBundleArguments.getBoolean("enableSecondEditTextField", false)) {
            if (mBundleArguments.containsKey("secondEditTextFieldHeader")) {
                ((ViewStub) view.findViewById(R.id.vs_tv_2)).inflate();
                TextView tv = ((TextView) view.findViewById(R.id.tv_2));

                tv.setText(mBundleArguments.getString("secondEditTextFieldHeader"));
                setHeaderTextViewAttributes(tv);
            }

            ((ViewStub) view.findViewById(R.id.vs_spc)).inflate();
            ((ViewStub) view.findViewById(R.id.vs_et)).inflate();

            et2 = (EditText) view.findViewById(R.id.et_2);

            et2.setText(mBundleArguments.getString("secondEditTextFieldInitialText", null));
        }

        String postButnText = mBundleArguments.getString("positiveButtonText", "OK");
        String negtButnText = mBundleArguments.getString("negativeButtonText", "Cancel");

        if (mBaseOnClickListener != null) {
            final EditText finalEt2 = et2;

            bldr.setPositiveButton(postButnText, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (mBaseOnClickListener instanceof OneFieldOnClickListener) {
                        ((OneFieldOnClickListener) mBaseOnClickListener).onClick(dialog, which,
                                mEt1.getText().toString());
                    } else {
                        ((TwoFieldsOnClickListener) mBaseOnClickListener).onClick(dialog, which,
                                mEt1.getText().toString(), finalEt2.getText().toString());
                    }
                }
            });

            bldr.setNegativeButton(negtButnText, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (mBaseOnClickListener instanceof OneFieldOnClickListener) {
                        ((OneFieldOnClickListener) mBaseOnClickListener).onClick(dialog, which,
                                mEt1.getText().toString());
                    } else {
                        ((TwoFieldsOnClickListener) mBaseOnClickListener).onClick(dialog, which,
                                mEt1.getText().toString(), finalEt2.getText().toString());
                    }
                }
            });
        } else {
            bldr.setPositiveButton(postButnText, null);
            bldr.setNegativeButton(negtButnText, null);
        }
        return bldr.create();
    }

    @Override
    public void onStart() {
        super.onStart();
        if (mOnDialogCreatedListener != null) {
            mOnDialogCreatedListener.onDialogCreated(getDialog());
        }

        UIUtils.ServicesUtils.showKeyboard(getActivity());
    }

    /**
     * Set the attributes for a header TextView.
     */
    private void setHeaderTextViewAttributes(TextView textView) {
        textView.setTextSize(15);
        textView.setTypeface(textView.getTypeface(), Typeface.BOLD);
    }

    @Override
    public void show() {
        show(mFragmentActivity.getSupportFragmentManager(), this.getClass().getSimpleName());
    }

    @Override
    public DialogFragment getFragment() {
        return this;
    }

    @Override
    public void setOnDialogCreatedListener(OnDialogCreatedListener onDialogCreatedListener) {
        mOnDialogCreatedListener = onDialogCreatedListener;
    }

    /**
     * Enable the second EditText field. This is used when the BaseOnClickListener is set to null.
     */
    public void enableSecondEditTextField() {
        getArguments().putBoolean("enableSecondEditTextField", true);
    }

    /**
     * Set the text of the positive button.
     */
    public void setPositiveButtonText(String positiveButtonText) {
        getArguments().putString("positiveButtonText", positiveButtonText);
    }

    /**
     * Set the text of the negative button.
     */
    public void setNegativeButtonText(String negativeButtonText) {
        getArguments().putString("negativeButtonText", negativeButtonText);
    }

    /**
     * Set the header of the first EditText field.
     */
    public void setFirstEditTextFieldHeader(String header) {
        getArguments().putString("firstEditTextFieldHeader", header);
    }

    /**
     * Set the header of the second EditText field.
     */
    public void setSecondEditTextFieldHeader(String header) {
        getArguments().putString("secondEditTextFieldHeader", header);
    }

    /**
     * Set the initial text of the first EditText field.
     */
    public void setFirstEditTextFieldInitialText(String text) {
        getArguments().putString("firstEditTextFieldInitialText", text);
    }

    /**
     * Set the initial text of the second EditText field.
     */
    public void setSecondEditTextFieldInitialText(String text) {
        getArguments().putString("secondEditTextFieldInitialText", text);
    }

    /**
     * OnClickListener used for the positive button
     */
    public interface BaseOnClickListener { }

    /**
     * OnClickListener for the positive button with one EditText input field
     */
    public interface OneFieldOnClickListener extends BaseOnClickListener {
        void onClick(DialogInterface dialog, int which, String text);
    }

    /**
     * OnClickListener for the positive button with two EditText input fields
     */
    public interface TwoFieldsOnClickListener extends BaseOnClickListener {
        void onClick(DialogInterface dialog, int which, String text1, String text2);
    }
    
}
