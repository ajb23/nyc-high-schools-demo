package com.nychighschoolsdemo.app.ui.global.dialog;

import android.app.Dialog;
import android.support.v4.app.DialogFragment;

/**
 * Base dialog interface for all dialogs.
 */
public interface BaseDialog {

    /**
     * Show the dialog.
     */
    void show();

    /**
     * Get the fragment of the dialog.
     * @return Fragment dialog
     */
    DialogFragment getFragment();

    /**
     * Set the OnDialogCreatedListener.
     */
    void setOnDialogCreatedListener(OnDialogCreatedListener onDialogCreatedListener);


    /**
     * A listener to retrieved the dialog after it has been created for post-creation operations. (It is necessary as
     * operations on an AlertDialog [e.g. getting one of its inner views or setting an onClickListener for one of its
     * buttons] outside onCreateDialog() will only work after onStart() has been called).
     */
    interface OnDialogCreatedListener {
        void onDialogCreated(Dialog dialog);
    }

}
