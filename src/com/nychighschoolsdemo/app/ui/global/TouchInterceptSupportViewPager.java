package com.nychighschoolsdemo.app.ui.global;

import android.content.Context;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import com.nychighschoolsdemo.app.util.MetricsUtils;

/**
 * Custom view pager of which touch interception can be
 * disabled for certain views, pages, swipe directions, or entirely.
 */
public class TouchInterceptSupportViewPager extends ViewPager {
    private boolean mInterceptDisabled;
    private View[] mTabViewsInterceptDisabled;
    private View mViewInterceptDisabled;
    // Internal classes
    private GestureDetectorCompat mSwipeGestureDetector;
    // Helper variables
    private int mPreviousPage;

    public TouchInterceptSupportViewPager(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    /**
     * Disallows the viewpager from intercepting touch events entirely (i.e. disables the view pager).
     * @param disableIntercept True if the viewpager is to not intercept touch events.
     */
    public void disableIntercept(boolean disableIntercept) {
        mInterceptDisabled = disableIntercept;
    }

    /**
     * Disables interception for touch events on a specific view.
     * @param view The view on which touch interception should be disabled.
     */
    public void disableInterceptForView(View view) {
        mViewInterceptDisabled = view;
    }

    /**
     * Specifies all the content views from a set of tabs (or tabstrip) on
     * which this ViewPager should be disallowed from intercepting touch events.
     * During a touch event on the first view, the ViewPager will be disallowed to intercept
     * touch events on a right swipe. For the last view, it will be on a left swipe.
     * @param tabViews The tab content views in order from first to last.
     */
    public void disableInterceptForTabViews(View[] tabViews) {
        mTabViewsInterceptDisabled = tabViews;
    }

//    /**
//     * Disallow the viewpager from intercepting touch events in a specific direction only (left or right).
//     * @param swipeView The view in which the swipe gesture is made on.
//     * @param disallowedSwipeDirection The direction of which to disallow the viewpager from intercept touch events.
//     * @param reallowAfterTouchEvent Whether or not to reallow the viewpager to intercept touch events after the touch event.
//     */
//    public void disableInterceptForSwipeDirection(final View swipeView, SwipeDirection disallowedSwipeDirection, boolean reallowAfterTouchEvent) {
//        mInterceptDisabled = true;
//        mTabViewsInterceptDisabled = null;
//        mDisallowedSwipeDirection = disallowedSwipeDirection;
//
//        final OnTouchListener emptyOnTouchListener = new OnTouchListener() {
//            @Override
//            public boolean onTouch(View view, MotionEvent motionEvent) { return false; }
//        };
//        swipeView.setOnTouchListener(new OnTouchListener() {
//            @Override
//            public boolean onTouch(View view, MotionEvent motionEvent) {
//                mSwipeGestureDetector.onTouchEvent(motionEvent);
//                swipeView.setOnTouchListener(emptyOnTouchListener);
//                return false;
//            }
//        });
//    }


    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        if (mInterceptDisabled) {
            return false;
        }

        if (mViewInterceptDisabled != null) {
            if (!MetricsUtils.inRegion(event.getRawX(), event.getRawY(), mViewInterceptDisabled)) {
                return super.onInterceptTouchEvent(event);
            }
        }

        if (mTabViewsInterceptDisabled != null) {
            boolean inDisallowedView = false;
            // Since this class maintains the previous page number and not the current, check if the
            //   user touched a view inside a tab with page number +- 1 to the previous page number.
            // Check the page before
            if (mPreviousPage - 1 != -1) {
                inDisallowedView = MetricsUtils.inRegion(event.getRawX(), event.getRawY(), mTabViewsInterceptDisabled[mPreviousPage - 1]);
            }
            // Check the page after
            if (!inDisallowedView && mPreviousPage + 1 != mTabViewsInterceptDisabled.length) {
                inDisallowedView = MetricsUtils.inRegion(event.getRawX(), event.getRawY(), mTabViewsInterceptDisabled[mPreviousPage + 1]);
            }
            if (inDisallowedView)
                return false;
        }
//            if (mTabViewsInterceptDisabled != null) {
//                float x1 = -1, x2 = -1;
//            final int MIN_DISTANCE = 150;
//            switch(event.getAction()) {
//                case MotionEvent.ACTION_DOWN:
//                    Log.d("", "intercept: ACTION_DOWN");
//                    x1 = event.getX();
//                    break;
//                case MotionEvent.ACTION_UP:
//                    Log.d("", "intercept: ACTION_UP");
//                    x2 = event.getX();
//                    float deltaX = x2 - x1;
//                    if (Math.abs(deltaX) > MIN_DISTANCE) {
//                        if (x2 > x1) {      // Left to right swipe
//                            Log.d("", "intercept: l to r");
//                        } else {            // Right to left swipe
//                            Log.d("", "intercept: r to l");
//                        }
//                    }
//                    break;
//            }
//        }
        return false;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
//        Log.d("", "intercept: 2");
        return super.onTouchEvent(event);
    }

    public void setCurrentPagePosition(int page) {
        mPreviousPage = page;
    }

    private class SwipeGestureDetector extends GestureDetector.SimpleOnGestureListener {
        private static final int SWIPE_MIN_DISTANCE = 120;
        private static final int SWIPE_MAX_OFF_PATH = 250;
        private static final int SWIPE_THRESHOLD_VELOCITY = 200;

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            try {
                if (Math.abs(e1.getY() - e2.getY()) > SWIPE_MAX_OFF_PATH && Math.abs(velocityX) <= SWIPE_THRESHOLD_VELOCITY)
                    return false;
                // right to left swipe
                if(e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE) {
//                    Log.d("", "intercept: Left Swipe");
                }  else if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE) {
//                    Log.d("", "intercept: Right Swipe");
                }
            } catch (Exception e) {
                // nothing
            }
            return false;
        }

        @Override
        public boolean onDown(MotionEvent e) {
            return true;
        }
    }

}
