package com.nychighschoolsdemo.app.ui.global.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import com.nychighschoolsdemo.app.util.MetricsUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * A touch intercept support ExpandableListView
 * <p></p>
 * This ExpandableListView allows touch events to be passed to views within itself.
 */
public class TouchInterceptSupportExpandableListView extends ExpandableListView {
    private boolean mIsScrollingDisabled;
    private List<View> mDisabledScrollViewList;
    private List<CoordinateBox> mDisabledScrollCoordinateBoxList;
    private int mDisabledScrollCoordinateBoxIndex;

    public TouchInterceptSupportExpandableListView(Context context) {
        super(context);
        init();
    }

    public TouchInterceptSupportExpandableListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public TouchInterceptSupportExpandableListView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    /**
     * Initialize the view.
     */
    private void init() {
        mDisabledScrollViewList = new ArrayList<>();
        mDisabledScrollCoordinateBoxList = new ArrayList<>();
    }

    /**
     * Add a disabled scroll view.
     */
    public void addDisabledScrollView(View view) {
        mDisabledScrollViewList.add(view);
    }

    /**
     * Clear the disabled scroll view list.
     */
    public void clearDisabledScrollViewList() {
        mDisabledScrollViewList.clear();
    }

    /**
     * Add a disabled scroll CoordinateBox.
     */
    public void addCoordinateBox(int xLeft, int xRight, int yTop, int yBottom) {
        if (mDisabledScrollCoordinateBoxList.size() <= mDisabledScrollCoordinateBoxIndex) {
            mDisabledScrollCoordinateBoxList.add(new CoordinateBox(xLeft, xRight, yTop,
                    yBottom));
        } else {
            CoordinateBox crbb = mDisabledScrollCoordinateBoxList.get(
                    mDisabledScrollCoordinateBoxIndex);
            crbb.mXLeft = xLeft;
            crbb.mXRight = xRight;
            crbb.mYTop = yTop;
            crbb.mYBottom = yBottom;
        }
        mDisabledScrollCoordinateBoxIndex++;
    }

    /**
     * Clear the disabled scroll CoordinateBox list.
     */
    public void clearCoordinateBoxList() {
        for (CoordinateBox disbScrollRange : mDisabledScrollCoordinateBoxList) {
            disbScrollRange.mXLeft = 0;
            disbScrollRange.mXRight = 0;
            disbScrollRange.mYTop = 0;
            disbScrollRange.mYBottom = 0;
        }
        mDisabledScrollCoordinateBoxIndex = 0;
    }

    /**
     * Disable or enable scrolling entirely.
     */
    public void disableScrolling(boolean scrollingDisabled) {
        mIsScrollingDisabled = scrollingDisabled;
    }

    // Todo: Possibly need to implement this
//    @Override
//    public boolean dispatchTouchEvent(MotionEvent event) {
//        final int actionMaskd = event.getActionMasked() & MotionEvent.ACTION_MASK;
//        if (actionMaskd == MotionEvent.ACTION_DOWN) {
//            return super.dispatchTouchEvent(event);
//        }
//        if (actionMaskd == MotionEvent.ACTION_MOVE) {
//            if (MetricsUtils.inRegion(event.getRawX(), event.getRawY(), mScrollDisabledView)) {
//                return true;
//            }
//        }
//        if (actionMaskd == MotionEvent.ACTION_UP) {
//            // Check if we are still within the same view
//            if (pointToPosition((int) event.getX(), (int) event.getY()) == mPosition) {
//                super.dispatchTouchEvent(event);
//            } else {
//                // Clear pressed state, cancel the action
//                setPressed(false);
//                invalidate();
//                return true;
//            }
//        }
//        return super.dispatchTouchEvent(event);
//    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        if (mDisabledScrollViewList.size() > 0) {
            for (View scrollDisbView : mDisabledScrollViewList) {
                if (scrollDisbView != null && MetricsUtils.inRegion(event.getRawX(), event.getRawY(), scrollDisbView)) {
                    //DevLogger.message("INSIDE ",  true);
                /*if (MotionEventCompat.getActionMasked(event) == MotionEvent.ACTION_DOWN) {
                    if (scrollDisbView instanceof ScrollSupportView) {
                        ScrollSupportView ssv = (ScrollSupportView) scrollDisbView;
                        if (!ViewCompat.canScrollVertically(scrollDisbView, -1)) {
                            if (ssv.wasLastScrollDirectionDown()) {
                                return super.onInterceptTouchEvent(event);
                            }
                        } else if (!ViewCompat.canScrollVertically(scrollDisbView, 1)) {
                            if (ssv.wasLastScrollDirectionUp()) {
                                return super.onInterceptTouchEvent(event);
                            }
                        }
                    }
                }*/
                    return false;
                }
            }
        }
        if (mDisabledScrollCoordinateBoxList.size() > 0) {
            for (CoordinateBox disbScrollRange : mDisabledScrollCoordinateBoxList) {
                float x = event.getRawX();
                float y = event.getRawY();
                if (x > disbScrollRange.mXLeft && x < disbScrollRange.mXRight && y > disbScrollRange.mYTop && y <
                        disbScrollRange.mYBottom) {
                    return false;
                }
            }
        }
        return super.onInterceptTouchEvent(event);
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        if (mIsScrollingDisabled) {
            return false;
        }
        return super.onTouchEvent(ev);
    }

    public static class CoordinateBox {
        public int mXLeft;
        public int mXRight;
        public int mYTop;
        public int mYBottom;

        public CoordinateBox(int xLeft, int xRight, int yTop, int yBottom) {
            mXLeft = xLeft;
            mXRight = xRight;
            mYTop = yTop;
            mYBottom = yBottom;
        }

    }

}
