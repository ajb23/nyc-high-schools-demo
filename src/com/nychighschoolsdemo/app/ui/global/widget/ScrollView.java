package com.nychighschoolsdemo.app.ui.global.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;

public class ScrollView extends android.widget.ScrollView implements ScrollSupportView {
    protected boolean mIsScrollingUp;
    protected boolean mIsScrollingDown;
    private boolean mWasLastScrollDirectionUp;
    private boolean mWasLastScrollDirectionDown;

    private float mOnTouchEventPreviousY;

    public ScrollView(Context context) {
        super(context);
        init();
    }

    public ScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ScrollView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    /**
     * Initialize the view.
     */
    private void init() {

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                mOnTouchEventPreviousY = event.getY();
                break;
            case MotionEvent.ACTION_UP:
                mWasLastScrollDirectionUp = mIsScrollingUp;
                mWasLastScrollDirectionDown = mIsScrollingDown;
                mIsScrollingUp = false;
                mIsScrollingDown = false;
                break;
            case MotionEvent.ACTION_MOVE:
                float newY = event.getY();
                float difY = mOnTouchEventPreviousY - newY;
                if (difY > 0) {
                    mIsScrollingUp = true;
                    mIsScrollingDown = false;
                } else if (difY < 0) {
                    mIsScrollingUp = false;
                    mIsScrollingDown = true;
                } else {
                    mIsScrollingUp = false;
                    mIsScrollingDown = false;
                }
                mOnTouchEventPreviousY = newY;
        }
        return super.onTouchEvent(event);
    }

    @Override
    public boolean isScrollingUp() {
        return mIsScrollingUp;
    }

    @Override
    public boolean isScrollingDown() {
        return mIsScrollingDown;
    }

    @Override
    public boolean wasLastScrollDirectionUp() {
        return mWasLastScrollDirectionUp;
    }

    @Override
    public boolean wasLastScrollDirectionDown() {
        return mWasLastScrollDirectionDown;
    }

}
