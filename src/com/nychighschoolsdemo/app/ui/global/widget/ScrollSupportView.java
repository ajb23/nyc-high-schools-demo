package com.nychighschoolsdemo.app.ui.global.widget;

/**
 * A scroll support view
 * <p></p>
 * This view helps get scroll data.
 */
public interface ScrollSupportView {

    boolean isScrollingUp();
    boolean isScrollingDown();
    boolean wasLastScrollDirectionUp();
    boolean wasLastScrollDirectionDown();

}
