package com.nychighschoolsdemo.app.ui.global.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import com.nychighschoolsdemo.app.util.MetricsUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * A touch intercept support ScrollView
 * <p></p>
 * This ScrollView allows touch events to be passed to views within itself.
 */
public class TouchInterceptSupportScrollView extends ScrollView {
    private boolean mIsScrollingDisabled;
    private List<View> mDisabledScrollViewList;

    public TouchInterceptSupportScrollView(Context context) {
        super(context);
        init();
    }

    public TouchInterceptSupportScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public TouchInterceptSupportScrollView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    /**
     * Initialize the view.
     */
    private void init() {
        mDisabledScrollViewList = new ArrayList<>();
    }

    /**
     * Disable or enable scrolling entirely.
     */
    public void disableScrolling(boolean scrollingDisabled) {
        mIsScrollingDisabled = scrollingDisabled;
    }

    /**
     * Add a disabled scroll view.
     */
    public void addDisabledScrollView(View view) {
        mDisabledScrollViewList.add(view);
    }

    /**
     * Clear the disabled scroll view list.
     */
    public void clearDisabledScrollViewList() {
        mDisabledScrollViewList.clear();
    }

    public List<View> getDisabledScrollViewList() {
        return mDisabledScrollViewList;
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
//        DevLogger.message("TISSV dispatchTouchEvent", true);
//        if (mEnable) {
//            // Note the returning true causes this and the ListView's dispatchTouchEvents to be called continuously
//            // and false causes only the ListView's dispatchTouchEvents to be called continuously.
//            return true;
//        }
        return super.dispatchTouchEvent(ev);
    }

    @Override
    public boolean onInterceptTouchEvent(final MotionEvent event) {
//        DevLogger.message("TISSV onInterceptTouchEvent", true);
        /*
        Note: The following actions occur for #onInterceptTouchEvent() and both boolean return types
          #onInterceptTouchEvent(): The touch event is controlled by this view but still passed to the child views.
          true: The touch event is controlled by this view and not at all passed to the child view.
          false: The touch event is not at all controlled by this view and passed to the child view.
         */
        if (mDisabledScrollViewList.size() > 0) {
            for (View scrollDisbView : mDisabledScrollViewList) {
                if (scrollDisbView != null && MetricsUtils.inRegion(event.getRawX(), event.getRawY(), scrollDisbView)) {
                    //if (MotionEventCompat.getActionMasked(event) == MotionEvent.ACTION_DOWN) {
                    if (scrollDisbView instanceof ExpandableListView) {
//                        ExpandableListView ssv = (ExpandableListView) scrollDisbView;
//                        if (ssv.mSv == null)
//                            ssv.setCView(TouchInterceptSupportScrollView.this);
//                            boolean canScrollUp = ViewCompat.canScrollVertically(scrollDisbView, -1);
//                            boolean canScrollDown = ViewCompat.canScrollVertically(scrollDisbView, 1);
//                            if (!canScrollUp) {
//                                DevLogger.message("Break 5");
//                                if (ssv.wasLastScrollDirectionDown()) {
//                                    DevLogger.message("Break 6");
//                                    return super.onInterceptTouchEvent(event);
//                                }
//                            } else if (!canScrollDown) {
//                                DevLogger.message("Break 5");
//                                if (ssv.wasLastScrollDirectionUp()) {
//                                    DevLogger.message("Break 6");
//                                    return super.onInterceptTouchEvent(event);
//                                }
//                            }
//                        DevLogger.message("mEnable: " + mEnable);
//                        if (mEnable) {
//                            return true;
//                        } else {
//                            return false;
//                        }
                    }
                    //}
                    //DevLogger.message("Break 7", true);
                    return false;
                }
            }
        }
        return super.onInterceptTouchEvent(event);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
//        DevLogger.message("TISSV onTouchEvent", true);
        if (mIsScrollingDisabled) {
            return false;
        }
//        switch (event.getAction()) {
//            case MotionEvent.ACTION_UP:
//                DevLogger.message("action up", true);
//                mEnable = false;
//                break;
//        }
        return super.onTouchEvent(event);
    }

}
