package com.nychighschoolsdemo.app.ui.global.widget;

import android.content.Context;
import android.graphics.Typeface;
import com.nychighschoolsdemo.app.base.Application;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Hashtable;

/**
 * Class to get typefaces from resources without causing memory leaks.
 * Note: Using Typeface#createFromAsset directly in code will cause memory leaks.
 */
public class Typefaces {
    private static final Logger Log = LoggerFactory.getLogger(Typefaces.class);
    private static final Hashtable<String, Typeface> cache = new Hashtable<>();
    private static Typeface sAppDefault = get(Application.getContext(), "Lato-Reg");

    public static Typeface getDefault() {
        return sAppDefault;
    }

    public static Typeface get(Context cntxt, String name) {
        synchronized (cache) {
            if (!cache.containsKey(name)) {
                try {
                    Typeface typeface = Typeface.createFromAsset(cntxt.getAssets(), String.format("fonts/%s.ttf", name));
                    cache.put(name, typeface);
                } catch (Exception e) {
                    Log.error("Could not get typeface '{}': {}", name, e.getMessage());
                    return null;
                }
            }
            return cache.get(name);
        }
    }

}
