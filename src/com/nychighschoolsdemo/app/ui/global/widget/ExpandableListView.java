package com.nychighschoolsdemo.app.ui.global.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;

/**
 * An expandable list view
 */
public class ExpandableListView extends android.widget.ExpandableListView implements ScrollSupportView {
    private boolean mIsScrollingUp;
    private boolean mIsScrollingDown;
    private boolean mWasLastScrollDirectionUp;
    private boolean mWasLastScrollDirectionDown;

    private float mOnTouchEventPreviousY;

    public TouchInterceptSupportScrollView mSv;

    public ExpandableListView(Context context) {
        super(context);
        init();
    }

    public ExpandableListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ExpandableListView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public void setCView(TouchInterceptSupportScrollView sv) {
        mSv = sv;
    }

    /**
     * Initialize the view.
     */
    private void init() {

    }

    // TODO: Need to test this without the outer ExpandableListView

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
//        DevLogger.message("ELV dispatchTouchEvent", true);
//        if (mSv != null && mSv.mEnable) {
//            DevLogger.message("Redispatching");
//            //mSv.dispatchTouchEvent(event);
//            return false;
//        }
        return super.dispatchTouchEvent(event);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
//        DevLogger.message("ELV onInterceptTouchEvent", true);
//        if (mSv != null && mSv.mEnable) {
//            return false;
//        }
        return super.onInterceptTouchEvent(ev);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
//        DevLogger.message("ELV onTouchEvent", true);
//        switch (event.getAction()) {
//            case MotionEvent.ACTION_DOWN:
//                mOnTouchEventPreviousY = event.getY();
//                break;
//            case MotionEvent.ACTION_UP:
//                mWasLastScrollDirectionUp = mIsScrollingUp;
//                mWasLastScrollDirectionDown = mIsScrollingDown;
//                mIsScrollingUp = false;
//                mIsScrollingDown = false;
//                break;
//            case MotionEvent.ACTION_MOVE:
//                float newY = event.getY();
//                float difY = mOnTouchEventPreviousY - newY;
//                if (difY > 0) {
//                    mIsScrollingUp = true;
//                    mIsScrollingDown = false;
//                } else if (difY < 0) {
//                    mIsScrollingUp = false;
//                    mIsScrollingDown = true;
//                } else {
//                    mIsScrollingUp = false;
//                    mIsScrollingDown = false;
//                }
//                mOnTouchEventPreviousY = newY;
//
//                if (mSv != null) {
//                    boolean canScrollUp = ViewCompat.canScrollVertically(this, 1);
//                    boolean canScrollDown = ViewCompat.canScrollVertically(this, -1);
//                    if (!canScrollUp && !canScrollDown) {
//                        mSv.dispatchTouchEvent(event);
//                        return false;
//                    } else if (mIsScrollingUp) {
////                        DevLogger.message("Break 3");
////                        if (!canScrollUp) {
////                            mSv.dispatchTouchEvent(event);
////                            return false;
////                        }
//                    } else if (mIsScrollingDown) {
//                        if (!canScrollDown) {
//                            DevLogger.message("Break 5");
//                            mSv.mEnable = true;
//                            return false;
//                        }
//                    }
//                }
//                break;
//        }
        return super.onTouchEvent(event);
    }

    @Override
    public boolean isScrollingUp() {
        return mIsScrollingUp;
    }

    @Override
    public boolean isScrollingDown() {
        return mIsScrollingDown;
    }

    @Override
    public boolean wasLastScrollDirectionUp() {
        return mWasLastScrollDirectionUp;
    }

    @Override
    public boolean wasLastScrollDirectionDown() {
        return mWasLastScrollDirectionDown;
    }

}
