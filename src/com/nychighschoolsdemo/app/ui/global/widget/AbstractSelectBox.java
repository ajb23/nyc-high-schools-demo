package com.nychighschoolsdemo.app.ui.global.widget;

import android.content.Context;
import android.util.AttributeSet;
import com.nychighschoolsdemo.app.util.DisplayUtils;

/**
 * The abstract select box
 */
public abstract class AbstractSelectBox extends TextView {

    public AbstractSelectBox(Context context) {
        super(context);
    }

    public AbstractSelectBox(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public AbstractSelectBox(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int widthSize = MeasureSpec.getSize(widthMeasureSpec);
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        int heightSize = MeasureSpec.getSize(heightMeasureSpec);
        int desrdHeight = DisplayUtils.dpToPixels(42);
        int height;
        if(heightMode == MeasureSpec.EXACTLY) {
            height = heightSize;
        } else if(heightMode == MeasureSpec.AT_MOST) {
            height = Math.min(desrdHeight, heightSize);
        } else {
            height = desrdHeight;
        }
        setMeasuredDimension(widthSize, height);
    }

}
