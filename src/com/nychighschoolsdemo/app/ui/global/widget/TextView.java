package com.nychighschoolsdemo.app.ui.global.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import com.nychighschoolsdemo.app.R;

/**
 * The application's main TextView widget for general use.
 */
public class TextView extends android.widget.TextView {
    static final String TAG = TextView.class.getSimpleName();
    static final int ATTR_TYPE_LINK = 1;
    static final int ATTR_FONT_STYLE_BOLD = 1;
    static final int ATTR_FONT_STYLE_ITALIC = 2;
    static final int ATTR_FONT_STYLE_MINOR_THIN = 3;
    static final int ATTR_FONT_STYLE_MAJOR_THIN = 4;
    static final int LINK_COLOR_NEUTRAL = Color.parseColor("#31357A");
    static final int LINK_COLOR_CLICKED = Color.parseColor("#000333");
    int mSelectedTypeAttribute;
    int mSelectedFontStyleAttribute;
    ViewGroup.MarginLayoutParams mLayoutParams;

    public TextView(Context context) {
        super(context);
        init(context, null);
    }

    public TextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public TextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        if (attrs != null) {
            setCustomAttributes(context, attrs);
        }
    }

    private void setCustomAttributes(Context ctx, AttributeSet attrs) {
        TypedArray typedArr = ctx.obtainStyledAttributes(attrs, R.styleable.TextView);
        mSelectedTypeAttribute = typedArr.getInt(R.styleable.TextView_type, 0);
        mSelectedFontStyleAttribute = typedArr.getInt(R.styleable.TextView_fontStyle, 0);
        typedArr.recycle();
        setUpFontStyleAttribute(ctx);
        setUpForTypeAttribute();
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        mLayoutParams = (LinearLayout.LayoutParams) getLayoutParams();
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        mLayoutParams = (ViewGroup.MarginLayoutParams) getLayoutParams();
    }

    /* ----  ----  ----  ----  ----  ----  ----  ----  ----  ----  ---
                 -------------------  Dynamic editing methods  -------------------
                 ----  ----  ----  ----  ----  ----  ----  ----  ----  ----  ----*/
    public void appendRedAsterisk() {
        String currentText = getText().toString();
        if((currentText.length() == 0) || (currentText.length() != 0) && (getText().charAt((getText().length() - 1)) != '*')) {
            SpannableString newTextWithRedAsterisk = new SpannableString(currentText + "*");
            newTextWithRedAsterisk.setSpan(new ForegroundColorSpan(ContextCompat.getColor(getContext(), R.color
                            .app_text_red)),
                    (newTextWithRedAsterisk.length() - 1), newTextWithRedAsterisk.length(), 0);
            setText(newTextWithRedAsterisk, TextView.BufferType.SPANNABLE);
        }
    }

    public void removeRedAsterisk() {
        int currentTextLength = getText().length();
        if((currentTextLength != 0) && (getText().charAt((getText().length() - 1)) == '*')) {
            setText(getText().subSequence(0, (getText().length() - 1)).toString());
        }
    }

    /* ----  ----  ----  ----  ----  ----  ----  ----  ----  ----  --
     -----------------------  Set up methods  -----------------------
     ----  ----  ----  ----  ----  ----  ----  ----  ----  ----  --*/
    private void setUpFontStyleAttribute(Context ctx) {
        switch (mSelectedFontStyleAttribute) {
            case 1: setTypeface(Typefaces.get(ctx, "Lato-Bol"));
                break;
            case 2: setTypeface(Typefaces.get(ctx, "Lato-RegIta"));
                break;
            case 3: setTypeface(Typefaces.get(ctx, "Lato-Lig"));
                break;
            case 4: setTypeface(Typefaces.get(ctx, "Lato-Hai"));
                break;
        }
        setTypeface(Typefaces.getDefault());
    }

    private void setUpForTypeAttribute() {
        if (mSelectedTypeAttribute == ATTR_TYPE_LINK) {
            setTextColor(LINK_COLOR_NEUTRAL);
            setOnTouchListener(mOnTouchListener);
        }
    }

    OnTouchListener mOnTouchListener = new OnTouchListener() {

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (mSelectedTypeAttribute == ATTR_TYPE_LINK) {
                int tX = (int) event.getX();
                int tY = (int) event.getY();
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        setTextColor(LINK_COLOR_CLICKED);
                        break;
                    case MotionEvent.ACTION_MOVE:
//                        DevLogger.print(tX + " " + tY + "  |  " + mLayoutParams.leftMargin + "  " + mLayoutParams.rightMargin);

//                        if (mDragImagePointTool) {
//                            mImgPointToolParams.leftMargin = xTouchCoord - mImgPointToolXCenterOffset;
//                            mImgPointToolParams.topMargin = yTouchCoord - mImgPointToolDragYOffset;
//                            checkImgPointToolBounds(true);
//                            mImgPointTool.setLayoutParams(mImgPointToolParams);
//
//
//                            // Figure this out -->
//                            //LogV.LogNum("test", (mImgPointToolParams.topMargin - mFrameLayoutYOffset), (mImgPointToolBounds.top));
//
//                        }
                        break;
                    case MotionEvent.ACTION_UP:
                        setTextColor(LINK_COLOR_NEUTRAL);
                        break;
                }
            }
            return false;
        }
    };

}
