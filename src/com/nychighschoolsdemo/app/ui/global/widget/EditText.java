package com.nychighschoolsdemo.app.ui.global.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.View;
import com.nychighschoolsdemo.app.R;
import com.nychighschoolsdemo.app.util.DisplayUtils;
import com.nychighschoolsdemo.app.util.GraphicsUtil;

/**
 * The application's main EditText widget for text input.
 * It can be assigned filters for amount, percentage and age.
 */
public class EditText extends android.widget.EditText {
    private static final int ATTR_FONT_STYLE_VAL_BOLD = 1;
    private static final int ATTR_FONT_STYLE_VAL_ITALIC = 2;
    private static final int ATTR_FONT_STYLE_VAL_MAJOR_THIN = 4;
    private static final int ATTR_FONT_STYLE_VAL_MINOR_THIN = 3;
    private static final int ATTR_INPUT_TYPE_VAL_AMOUNT = 1;
    private static final int ATTR_INPUT_TYPE_VAL_PERCENT = 2;
    private static final int ATTR_INPUT_TYPE_VAL_AGE = 3;
    private static final int ATTR_LINE_TYPE_VAL_SINGLE = 1;
    private static final int ATTR_LINE_TYPE_VAL_MULTIPLE = 2;
    private int mAttrInputType;
    private int mAttrLineType;
    private float mAttrMaximum;
    private Drawable mDrawablePercentBlack;
    private Drawable mDrawablePercentRed;
    private SimpleTextChangedListener mSimpleTextChangedListener;
    private FilterOnTextChangedListener mFilterOnTextChangedListener;
    private int mPdngVert;
    private final int mTextSize = 16;

    public EditText(Context context) {
        super(context);
        init(context, null);
    }

    public EditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public EditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        if (attrs != null) {
            setCustomAttributes(context, attrs);
        }

        //setBackgroundResource(R.drawable.wgt_bg_edittext);
        int wgtPdng = context.getResources().getDimensionPixelSize(R.dimen.text_view_box_padding);
        setPadding(wgtPdng, 0, wgtPdng, 0);
        //setGravity(Gravity.CENTER_VERTICAL);
        setTextSize(16);

        if (mAttrInputType == ATTR_INPUT_TYPE_VAL_AMOUNT) {
            enableAmountInputFilter();
            setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
        } else if(mAttrInputType == ATTR_INPUT_TYPE_VAL_PERCENT) {
            enablePercentInputFilter();
            setInputType(InputType.TYPE_CLASS_NUMBER);
        } else if(mAttrInputType == ATTR_INPUT_TYPE_VAL_AGE) {
            enableAgeInputFilter();
            setInputType(InputType.TYPE_CLASS_NUMBER);
        }

        if (mAttrLineType == ATTR_LINE_TYPE_VAL_MULTIPLE) {
            setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_MULTI_LINE);
        } else {
            // set to single-line as multi-line is default
            //setSingleLine(true);
        }

        if (mAttrInputType == ATTR_INPUT_TYPE_VAL_PERCENT) {
            TextView vTxt = new TextView(context);
            vTxt.setBackgroundColor(Color.TRANSPARENT);
            vTxt.setTextColor(Color.BLACK);
            vTxt.setTextSize(mTextSize);
            vTxt.setText("%");
            Bitmap bmp = GraphicsUtil.viewToBitmap(vTxt);
            mDrawablePercentBlack = GraphicsUtil.bitmapToDrawable(bmp);
            setCompoundDrawablesWithIntrinsicBounds(null, null, mDrawablePercentBlack, null);
            vTxt.setTextColor(Color.BLACK);
            bmp = GraphicsUtil.viewToBitmap(vTxt);
            mDrawablePercentRed = GraphicsUtil.bitmapToDrawable(bmp);
        }

        if (mAttrInputType == ATTR_INPUT_TYPE_VAL_AMOUNT) {
            setHint("$  0.00");
        }
    }

    private void setCustomAttributes(Context ctx, AttributeSet attrs) {
        TypedArray typedArr = ctx.obtainStyledAttributes(attrs, R.styleable.EditText);
        mAttrInputType = typedArr.getInt(R.styleable.EditText_inputType, -1);
        mAttrLineType = typedArr.getInt(R.styleable.EditText_lineType, -1);
        mAttrMaximum = typedArr.getFloat(R.styleable.EditText_lineType, -1);

        typedArr.recycle();
        typedArr = ctx.obtainStyledAttributes(attrs, R.styleable.TextView);
        int attrFontStyle = typedArr.getInt(R.styleable.TextView_fontStyle, -1);
        typedArr.recycle();

        switch (mAttrInputType) {
            case 1: mAttrInputType = ATTR_INPUT_TYPE_VAL_AMOUNT;
                break;
            case 2: mAttrInputType = ATTR_INPUT_TYPE_VAL_PERCENT;
                break;
            case 3: mAttrInputType = ATTR_INPUT_TYPE_VAL_AGE;
                break;
        }

        switch (mAttrLineType) {
            case 1: mAttrLineType = ATTR_LINE_TYPE_VAL_SINGLE;
                break;
            case 2: mAttrLineType = ATTR_LINE_TYPE_VAL_MULTIPLE;
                break;
        }

        switch (attrFontStyle) {
            case 1: setTypeface(Typefaces.get(ctx, "Lato-Bol"));
                break;
            case 2: setTypeface(Typefaces.get(ctx, "Lato-RegIta"));
                break;
            case 3: setTypeface(Typefaces.get(ctx, "Lato-Lig"));
                break;
            case 4: setTypeface(Typefaces.get(ctx, "Lato-Hai"));
                break;
        }

        setTypeface(Typefaces.getDefault());
    }

    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int widthSize = View.MeasureSpec.getSize(widthMeasureSpec);
        int heightMode = View.MeasureSpec.getMode(heightMeasureSpec);
        int heightSize = View.MeasureSpec.getSize(heightMeasureSpec);
        int desiredHeight;

        // The height is currently set for the EditText when using multi-line mode because its height does not adjust
        // automatically unless it is coded to do so. This should be done in the future.
        if (mAttrLineType == ATTR_LINE_TYPE_VAL_MULTIPLE) {
            desiredHeight = DisplayUtils.dpToPixels(120);
        } else {
            desiredHeight = DisplayUtils.dpToPixels(44);
        }

        desiredHeight += (mPdngVert * 2);
        int height;

        if (heightMode == MeasureSpec.EXACTLY) {
            height = heightSize;
        } else if (heightMode == MeasureSpec.AT_MOST) {
            height = Math.min(desiredHeight, heightSize);
        } else {
            height = desiredHeight;
        }

        setMeasuredDimension(widthSize, height);
    }

    public void addSimpleTextChangedListener(final SimpleTextChangedListener simpleTextChangedListener) {
        mSimpleTextChangedListener = simpleTextChangedListener;

        addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) { }
            @Override
            public void afterTextChanged(Editable s) {
                simpleTextChangedListener.onTextChanged(EditText.this, s);
            }
        });
    }

    public void addFilterOnTextChangedListener(FilterOnTextChangedListener filterOnTextChangedListener) {
        mFilterOnTextChangedListener = filterOnTextChangedListener;
    }

    /* ----  ----  ----  ----  ----  ----  ----  ----  ---
     ---------------------  Getters  ---------------------
     ----  ----  ----  ----  ----  ----  ----  ----  ---*/
    public int getIntValue() {
        if((!isEmpty()) && (mAttrInputType != 0)) {
            return Math.round(Float.parseFloat(getText().toString()));
        }
        return 0;
    }
    public float getFloatValue() {
        if((!isEmpty()) && (mAttrInputType != 0)) {
            return Float.parseFloat(getText().toString());
        }
        return 0;
    }
    public String getString() {
        return getText().toString();
    }
    public boolean isEmpty() {
        return (getText().length() == 0);
    }


    /* ----  ----  ----  ----  ----  ----  ----  ----  ---
     ---------------------  Filters  ---------------------
     ----  ----  ----  ----  ----  ----  ----  ----  ---*/
    private void enableAmountInputFilter() {
        final EditText editText = this;
        addTextChangedListener(new TextWatcher() {
            int mCount;
            int mCurrentSelection;
            int mCurrentTextColor;
            int mPrevDecimalIndex;
            String mPrevEdttxtString;

            @Override
            public void beforeTextChanged(CharSequence charSequence, int start, int count, int after) {
                mPrevEdttxtString = charSequence.toString();
                mPrevDecimalIndex = mPrevEdttxtString.indexOf(".");
                mCurrentSelection = start;
                mCount = count;
                if(mCurrentTextColor != Color.BLACK) {
                    editText.setTextColor(Color.BLACK);
                    setCompoundDrawablesWithIntrinsicBounds(null, null, mDrawablePercentBlack, null);
                }
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) { }
            @Override
            public void afterTextChanged(Editable editable) {
                editText.removeTextChangedListener(this);
                String edttxtString = editable.toString();
                String[] edttxtSplitString = edttxtString.split("[.]");
                int prevEdttxtStringLengthDiff = mPrevEdttxtString.length() - edttxtString.length();
                /* --------------------  Deletion/select-replace cases  -------------------- */
                // If one or more characters were deleted (number and/or
                //   decimal), allow the system to delete it/them and respond accordingly.
                if (prevEdttxtStringLengthDiff >= 0) {
                    char replaceCharacter = '\0';
                    // Check if the one or more potentially selected characters
                    //   were replaced by a character. If so, store the character.
                    if (mCount != prevEdttxtStringLengthDiff) {
                        replaceCharacter = edttxtString.charAt(mCurrentSelection);
                    }
                    // If a decimal existed and was deleted, also delete the numbers
                    //   after the decimal but reinsert the replace character if there was one.
                    if (mPrevDecimalIndex != -1 && (edttxtSplitString.length != 2 || replaceCharacter == '.')) {
                        String beforeDeletionSubstring = mPrevEdttxtString.substring(0, mCurrentSelection);
                        if (replaceCharacter == '\0')
                            editText.setText(beforeDeletionSubstring);
                        else
                            editText.setText(beforeDeletionSubstring + replaceCharacter);
                        edttxtString = editText.getText().toString();
                        editText.setSelection(editText.getText().toString().length());
                    }
                    // Else if a decimal did not exist and a decimal was inserted in
                    //   place of the selected character(s), allow the system to remove
                    //   the characters and handle adding the decimal accordingly.
                    else if (mPrevDecimalIndex == -1 && replaceCharacter == '.') {
                        // If the end index of the selected characters is within the correct range
                        //   for adding the decimal place, allow the decimal to be put in place
                        //   of the selected characters and move the selected index to the end.
                        if (mCurrentSelection + mCount >= mPrevEdttxtString.length() - 2) {
                            editText.setSelection(editText.getText().toString().length());
                        }
                        // Else manually remove the selected character(s) (because the system will automatically
                        //   put a decimal in its/their place) and move the selected index at the end.
                        else {
                            String beforeDeletionString = mPrevEdttxtString.substring(0, mCurrentSelection);
                            String afterDeletionString = mPrevEdttxtString.substring(mCurrentSelection + mCount);
                            editText.setText(beforeDeletionString + afterDeletionString);
                            edttxtString = editText.getText().toString();
                            editText.setSelection(editText.getText().toString().length());
                        }
                    }
                    if(mAttrMaximum != -1 && !edttxtString.isEmpty() && Float.parseFloat(edttxtString) > mAttrMaximum) {
                        editText.setTextColor(ContextCompat.getColor(getContext(), R.color.app_text_red));
                    }
                    editText.addTextChangedListener(this);
                    if(mFilterOnTextChangedListener != null) {
                        mFilterOnTextChangedListener.onTextChanged(editText);
                    }
                    return;
                }
                /* --------------------  Addition cases  -------------------- */
                // Cases when a decimal or a number after the decimal was added.
                if (edttxtSplitString.length == 2 && edttxtSplitString[1].length() > 2) {
                    // If a decimal was inserted in the wrong place, move
                    //   the selected index to two indexes before the last.
                    if (mPrevDecimalIndex == -1 && prevEdttxtStringLengthDiff == -1) {
                        editText.setText(mPrevEdttxtString);
                        editText.setSelection(mPrevEdttxtString.length() - 2);
                    }
                    // Else since a number was added after the decimal place making
                    //   the decimal-part too big, remove the added number and do nothing.
                    else {
                        editText.setText(mPrevEdttxtString);
                        editText.setSelection(mCurrentSelection);
                    }
                }
                // If the integer-part gets to the billions
                //   place, remove the added number and do nothing.
                if((edttxtSplitString.length > 0) && (edttxtSplitString[0].length() > 9)) {
                    editText.setText(mPrevEdttxtString);
                    editText.setSelection(mCurrentSelection);
                }
                if(mAttrMaximum != -1 && !edttxtString.isEmpty() && Float.parseFloat(edttxtString) > mAttrMaximum) {
                    editText.setTextColor(ContextCompat.getColor(getContext(), R.color.app_text_red));
                }
                editText.addTextChangedListener(this);
                if(mFilterOnTextChangedListener != null) {
                    mFilterOnTextChangedListener.onTextChanged(editText);
                }
            }
        });
    }

    private void enablePercentInputFilter() {
        final EditText editText = this;
        addTextChangedListener(new TextWatcher() {
            int mCurrentSelection;
            int mCurrentTextColor;
            String mPrevEdttxtString;

            public void beforeTextChanged(CharSequence charSequence, int start, int count, int after) {
                mPrevEdttxtString = charSequence.toString();
                mCurrentSelection = start;
                if(mCurrentTextColor != Color.BLACK) {
                    editText.setTextColor(Color.BLACK);
                    setCompoundDrawablesWithIntrinsicBounds(null, null, mDrawablePercentBlack, null);
                }
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) { }

            public void afterTextChanged(Editable editable) {
                editText.removeTextChangedListener(this);
                String edttxtString = editable.toString();
                int edttxtStringLength = edttxtString.length();
                if(edttxtStringLength == 3) {
                    if(Integer.parseInt(edttxtString) > 100) {
                        editText.setTextColor(ContextCompat.getColor(getContext(), R.color.app_text_red));
                        editText.setCompoundDrawablesWithIntrinsicBounds(null, null, mDrawablePercentRed, null);
                    }
                } else if(edttxtStringLength > 3) {
                    editText.setText(mPrevEdttxtString);
                    editText.setSelection(mCurrentSelection);
                    if(Integer.parseInt(mPrevEdttxtString) > 100) {
                        editText.setTextColor(ContextCompat.getColor(getContext(), R.color.app_text_red));
                        editText.setCompoundDrawablesWithIntrinsicBounds(null, null, mDrawablePercentRed, null);
                    }
                }
                editText.addTextChangedListener(this);
                if(mFilterOnTextChangedListener != null) {
                    mFilterOnTextChangedListener.onTextChanged(editText);
                }
            }
        });
    }

    private void enableAgeInputFilter() {
        final EditText editText = this;
        addTextChangedListener(new TextWatcher() {
            int mCurrentSelection;
            int mCurrentTextColor;
            String mPrevEdttxtString;

            public void beforeTextChanged(CharSequence charSequence, int start, int count, int after) {
                mPrevEdttxtString = charSequence.toString();
                mCurrentSelection = start;
                if(mCurrentTextColor != Color.BLACK) {
                    editText.setTextColor(Color.BLACK);
                }
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) { }

            public void afterTextChanged(Editable editable) {
                editText.removeTextChangedListener(this);
                String edttxtString = editable.toString();
                int edttxtStringLength = edttxtString.length();
                if(edttxtStringLength > 0) {
                    int integer = Integer.parseInt(edttxtString);
                    if((edttxtStringLength == 1) && (integer == 0)) {
                        editText.setTextColor(ContextCompat.getColor(getContext(), R.color.app_tabs_bar_indicator_light));
                    } else if((edttxtStringLength == 3) && (integer > 121)) {
                        editText.setTextColor(ContextCompat.getColor(getContext(), R.color.app_text_red));
                    } else if(edttxtStringLength > 3) {
                        editText.setText(mPrevEdttxtString);
                        editText.setSelection(mCurrentSelection);
                        int prevInteger = Integer.parseInt(mPrevEdttxtString);
                        if((prevInteger < 1) || (prevInteger > 121)) {
                            editText.setTextColor(ContextCompat.getColor(getContext(), R.color.app_text_red));
                        }
                    }
                }
                editText.addTextChangedListener(this);
                if(mFilterOnTextChangedListener != null) {
                    mFilterOnTextChangedListener.onTextChanged(editText);
                }
            }
        });
    }

    public interface SimpleTextChangedListener {
        void onTextChanged(View v, Editable editable);
    }

    public interface FilterOnTextChangedListener {
        void onTextChanged(EditText paramEditText);
    }

}
