package com.nychighschoolsdemo.app.ui.home;

import android.os.Bundle;
import com.nychighschoolsdemo.app.R;
import com.nychighschoolsdemo.app.base.AppSharedPreferences.APP_STATE;
import com.nychighschoolsdemo.app.ui.BaseActivity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The home activity
 */
public class HomeActivity extends BaseActivity {
    private static final Logger LOGGER = LoggerFactory.getLogger(HomeActivity.class);
    protected NycHighSchoolsFragment mNycHighSchoolsFragment;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        mNycHighSchoolsFragment = new NycHighSchoolsFragment();
        initActivity(HomeActivity.class, mNycHighSchoolsFragment, R.menu.home_activity);
        super.onCreate(savedInstanceState);

        // App state to keep track of closed activities
        APP_STATE.get().edit().putBoolean(APP_STATE.lifecycle_hasHomeActivityBeenDestroyed, false);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onDestroy() {
        APP_STATE.get().edit().putBoolean(APP_STATE.lifecycle_hasHomeActivityBeenDestroyed, true);
        super.onDestroy();
    }

}
