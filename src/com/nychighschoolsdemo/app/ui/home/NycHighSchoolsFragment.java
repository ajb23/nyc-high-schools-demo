package com.nychighschoolsdemo.app.ui.home;

import android.app.ProgressDialog;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.TextView;
import com.nychighschoolsdemo.app.R;
import com.nychighschoolsdemo.app.base.AppSharedPreferences.HTTP_REQUEST_METADATA;
import com.nychighschoolsdemo.app.base.event.HttpResponseEvent;
import com.nychighschoolsdemo.app.model.HighSchool;
import com.nychighschoolsdemo.app.model.SatScores;
import com.nychighschoolsdemo.app.rest.BaseHttpRequestError;
import com.nychighschoolsdemo.app.rest.EndpointsCaller;
import com.nychighschoolsdemo.app.rest.EndpointsData;
import com.nychighschoolsdemo.app.ui.BaseFragment;
import com.nychighschoolsdemo.app.ui.global.dialog.InfoDialog;
import com.nychighschoolsdemo.app.util.UIUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * The NYC high school list fragment
 */
public class NycHighSchoolsFragment extends BaseFragment {
    private MListAdapter mListAdapter;
    public ProgressDialog mProgressDialog;
    private ExpandableListView mExpandableListView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        initFragment(NycHighSchoolsFragment.class);
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.nyc_high_schools_o_fragment, container, false);
        mExpandableListView = (ExpandableListView) view.findViewById(android.R.id.list);
        mListAdapter = new MListAdapter(getActivity(), new ArrayList<HighSchool>());
        mExpandableListView.setAdapter(mListAdapter);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        boolean wasGetHighSchoolReqsMade = HTTP_REQUEST_METADATA.get().getLong(
                HTTP_REQUEST_METADATA.getHighSchools_responseReceivedElapsedRealtime, 0) != 0;
        boolean wasGetSatScoresReqsMade = HTTP_REQUEST_METADATA.get().getLong(
                HTTP_REQUEST_METADATA.getSatScores_responseReceivedElapsedRealtime, 0) != 0;
        if (!wasGetHighSchoolReqsMade || !wasGetSatScoresReqsMade) {
            mProgressDialog = ProgressDialog.show(NycHighSchoolsFragment.this.getContext(), null,
                    "Loading NYC high schools...");
            EndpointsCaller.getInstance().makeGetHighSchoolsRequest(new EndpointsCaller.OnRequestsFinishedListener() {
                @Override
                public void onRequestFinished(BaseHttpRequestError baseHttpRequestError) {
                    if (baseHttpRequestError == null) {
                        makeGetSatScoresRequestHelper();
                    } else {
                        InfoDialog.newInstance(getActivity(), "Failed to make request.",
                                baseHttpRequestError.getMessage()).show();
                    }
                }
            });
        } else {
            onRequestsFinished();
        }
    }

    private void onRequestsFinished() {
        UIUtils.DialogUtils.dismissDialog(mProgressDialog);

        List<HighSchool> highSchlList = EndpointsData.getHighSchoolList();
        Collections.sort(highSchlList, new Comparator<HighSchool>() {
            @Override
            public int compare(HighSchool highSchl1, HighSchool highSchl2) {
                return highSchl1.getSchoolName().compareTo(highSchl2.getSchoolName());
            }
        });
        mListAdapter = new MListAdapter(getActivity(), highSchlList);
        mExpandableListView.setAdapter(mListAdapter);
    }

    private void makeGetSatScoresRequestHelper() {
        EndpointsCaller.getInstance().makeGetSatScoresRequest(new EndpointsCaller.OnRequestsFinishedListener() {
            @Override
            public void onRequestFinished(BaseHttpRequestError baseHttpRequestError) {
                if (HTTP_REQUEST_METADATA.get().getLong(
                        HTTP_REQUEST_METADATA.getHighSchools_responseReceivedElapsedRealtime, 0) != 0) {
                    UIUtils.DialogUtils.dismissDialog(mProgressDialog);

                    if (baseHttpRequestError == null) {
                        onRequestsFinished();
                    } else {
                        InfoDialog.newInstance(getActivity(), "Failed to make request.",
                                baseHttpRequestError.getMessage()).show();
                    }
                }
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public void onEvent(HttpResponseEvent event) {
//        EndpointsCaller.ENDPOINTS_REQUEST_TYPE endpReqsType = event.getEndpointsRequestType();
//        if (endpReqsType == EndpointsCaller.ENDPOINTS_REQUEST_TYPE.GET_BASE_DATA ||
//                endpReqsType == EndpointsCaller.ENDPOINTS_REQUEST_TYPE.GET_SUITES_IN_SPATIAL_RANGE_FOR_USER_AREA) {
//            if (event.getBaseRequestError() != null) {
//                HTTP_REQUEST_METADATA.get().edit().remove(
//                        HTTP_REQUEST_METADATA.getHighSchoolsInSpatialRangeForUserArea_responseReceivedElapsedRealtime).apply();
//            }
//
//            updateViews();
//        }
    }

    protected static class MListAdapter extends BaseExpandableListAdapter {
        private LayoutInflater mLayoutInflater;
        private List<HighSchool> mHighSchoolList;

        public MListAdapter(FragmentActivity activity, List<HighSchool> highSchoolList) {
            mLayoutInflater = LayoutInflater.from(activity);
            mHighSchoolList = highSchoolList;
        }

        @Override
        public long getGroupId(int groupPosition) {
            return 0;
        }

        @Override
        public long getChildId(int groupPosition, int childPosition) {
            return 0;
        }

        @Override
        public boolean hasStableIds() { return false; }

        @Override
        public Object getGroup(int groupPosition) {
            return mHighSchoolList.get(groupPosition);
        }

        @Override
        public Object getChild(int groupPosition, int childPosition) {
            return new Object();
        }

        @Override
        public int getGroupCount() {
            return mHighSchoolList.size();
        }

        @Override
        public int getChildrenCount(int groupPosition) {
            return 1;
        }

        @Override
        public View getGroupView(int groupPosition, boolean isExpanded, View layoutView, ViewGroup parent) {
            if (layoutView == null) {
                layoutView = mLayoutInflater.inflate(R.layout.nyc_high_schools_s_list_s_adapter_o_group_view_layout,
                        parent, false);
            }
            HighSchool highSchl = mHighSchoolList.get(groupPosition);
            TextView tvName = (TextView) layoutView.findViewById(R.id.tv_name);
            tvName.setText(highSchl.getSchoolName());
            return layoutView;
        }

        private class ViewHolder {
            public TextView tvLocation;
            public TextView tvSatCriticalReadingAverageScore;
            public TextView tvSatMathAverageScore;
            public TextView tvSatWritingAverageScore;
        }

        @Override
        public View getChildView(int groupPosition, final int childPosition, boolean isLastChild, View convertView,
                                 ViewGroup parent) {
            if (convertView == null) {
                convertView = mLayoutInflater.inflate(R.layout.nyc_high_schools_s_list_s_adapter_o_child_view_layout,
                        parent, false);
                ViewHolder vHold = new ViewHolder();
                vHold.tvLocation = (TextView) convertView.findViewById(R.id.tv_location);
                vHold.tvSatCriticalReadingAverageScore = (TextView) convertView.findViewById(R.id.tv_score_1);
                vHold.tvSatMathAverageScore = (TextView) convertView.findViewById(R.id.tv_score_2);
                vHold.tvSatWritingAverageScore = (TextView) convertView.findViewById(R.id.tv_score_3);
                convertView.setTag(vHold);
            }

            // Disable the convert view rather than disabling all of the list's items in #isEnabled for the reason
            // explained in #isEnabled.
            convertView.setEnabled(false);

            HighSchool highSchl = mHighSchoolList.get(groupPosition);
            SatScores satScores = highSchl.getSatScores();
            ViewHolder vHold = (ViewHolder) convertView.getTag();
            vHold.tvLocation.setText("Boro: " + highSchl.getBoro());
            if (satScores != null &&
                    !(satScores.getCriticalReadingAverageScore() == 0 && satScores.getMathAverageScore() == 0 &&
                            satScores.getWritingAverageScore() == 0)) {
                vHold.tvSatCriticalReadingAverageScore.setText("Critical reading average SAT score: " +
                        satScores.getCriticalReadingAverageScore());
                vHold.tvSatMathAverageScore.setText("Math average SAT score: " + satScores.getMathAverageScore());
                vHold.tvSatWritingAverageScore.setText("Writing average SAT score: " +
                        satScores.getWritingAverageScore());
                vHold.tvSatMathAverageScore.setVisibility(View.VISIBLE);
                vHold.tvSatWritingAverageScore.setVisibility(View.VISIBLE);
            } else {
                vHold.tvSatCriticalReadingAverageScore.setText("No SAT data for this school.");
                vHold.tvSatMathAverageScore.setVisibility(View.GONE);
                vHold.tvSatWritingAverageScore.setVisibility(View.GONE);
            }
            return convertView;
        }

        @Override
        public boolean isChildSelectable(int groupPosition, int childPosition) {
            // List item cannot be disabled in Android 5.0 and greater because there is a bug causing their dividers to
            // not be shown. The row view must be disabled instead.
            return Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP;
        }
    }

}
