package com.nychighschoolsdemo.app.util;

import android.content.Context;
import android.content.Intent;

/**
 * Android utilities
 */
public class AndroidUtils {

    /**
     * Android intent utilities
     */
    public static final class IntentUtils {

        /**
         * Start an activity on a new stack.
         */
        public static void startActivityOnNewStack(Context context, Class activityClass) {
            Intent intent = new Intent();
            intent.setClass(context, activityClass);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        }

    }

}
