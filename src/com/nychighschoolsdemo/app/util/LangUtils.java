package com.nychighschoolsdemo.app.util;

import java.util.*;

/**
 * Language utilities
 */
public class LangUtils {
    static final Random RANDOM = new Random();

    public static final class ArrayUtils {

        public static <T> T[] concatenateArrays(T[] first, T[]... remaining) {
            int len = first.length;
            for (T[] array : remaining) {
                len += array.length;
            }
            T[] finalArr = Arrays.copyOf(first, len);
            int offset = first.length;
            for (T[] array : remaining) {
                System.arraycopy(array, 0, finalArr, offset, array.length);
                offset += array.length;
            }
            return finalArr;
        }

        /**
         * Append an item to an integer array.
         * @param array The array to append to
         * @param item The item to append
         * @return The array with a length of 1 greater with the last element being the appended item
         */
        public static int[] appendItemToIntegerArray(int[] array, int item) {
            int[] newArray = Arrays.copyOf(array, array.length + 1);
            newArray[newArray.length - 1] = item;
            return newArray;
        }

    }

    /**
     * Returns a pseudo-random number between min and max inclusive.
     * The difference between min and max can be at most
     * <code>Integer.MAX_VALUE - 1</code>.
     */
    public static int randInt(int min, int max) {
        return RANDOM.nextInt((max - min) + 1) + min;
    }

    /**
     * Get the keys of a Map by value.
     */
    public static <T, E> Set<T> getKeysByValue(Map<T, E> map, E value) {
        Set<T> keys = new HashSet<T>();
        for (Map.Entry<T, E> entry : map.entrySet()) {
            if (Objects.equals(value, entry.getValue())) {
                keys.add(entry.getKey());
            }
        }
        return keys;
    }

    /**
     * Language compare utilities
     */
    public static final class CompareUtils {

        /**
         * Check if two objects match.
         * @throws ClassCastException If the class type of obje2 does not match the class type of obje1
         * @throws IllegalArgumentException If a comparison for the object's class type is not implemented
         */
        public static boolean doObjectsMatch(Object obje1, Object obje2) throws ClassCastException,
                IllegalArgumentException {
            return compareObjects(obje1, obje2) == 0;
        }

        /**
         * Compare two objects.
         * @return -1 if object 1 is less than object 2, 0 if object 1 is equal to object 2 and 1 if object 1 is greater
         *         than object 2
         * @throws ClassCastException If the class type of obje2 does not match the class type of obje1
         * @throws IllegalArgumentException If a comparison for the object's class type is not implemented
         */
        public static int compareObjects(Object obje1, Object obje2) throws ClassCastException,
                IllegalArgumentException {
            int compResl;
            if (obje1 instanceof Integer) {
                compResl = ((Integer) obje1).compareTo((Integer) obje2);
            } else if (obje1 instanceof Long) {
                compResl = ((Long) obje1).compareTo((Long) obje2);
            } else if (obje1 instanceof Double) {
                compResl = ((Double) obje1).compareTo((Double) obje2);
            } else if (obje1 instanceof String) {
                compResl = ((String) obje1).compareTo((String) obje2);
            } else if (obje1 instanceof Date) {
                compResl = ((Date) obje1).compareTo((Date) obje2);
            } else {
                throw new IllegalArgumentException("Object class type not found");
            }
            return compResl;
        }

    }

}
