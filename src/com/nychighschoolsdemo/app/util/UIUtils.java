package com.nychighschoolsdemo.app.util;

import android.app.Dialog;
import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.nychighschoolsdemo.app.ui.global.dialog.BaseDialog;

public class UIUtils {

    public static class ViewUtils {

        public static class ListViewUtils {

            /**
             * Set the height of a ListView based on the height of its children.
             * @param listView
             * @param beforeChildViewsContentSet Whether or not the set the height of the ListView based on the height
             *                                   of its children before content is set on their views
             * @param listViewWidthPx The number of pixels the width of the ListView will be; this is irrelative if
             *                       beforeChildViewsContentSet is false
             * @author (http://kk-brothers.blogspot.com/2011/09/dynamically-change-listview-height.html)
             */
            private static void setListViewHeightBasedOnChildren(ListView listView, boolean beforeChildViewsContentSet,
                                                                 int listViewWidthPx) {
                ListAdapter listAdpt = listView.getAdapter();
                int listViewItemViewWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(),
                        View.MeasureSpec.UNSPECIFIED);
                int totalHght = 0;
                for (int i = 0; i < listAdpt.getCount(); i++) {
                    View listViewItemView = listAdpt.getView(i, null, listView);

                    if (i == 0) {
                        listViewItemView.setLayoutParams(new ViewGroup.LayoutParams(listViewItemViewWidth,
                                ViewGroup.LayoutParams.WRAP_CONTENT));
                    }

                    if (beforeChildViewsContentSet) {
                        listViewItemView.measure(listViewItemViewWidth, View.MeasureSpec.UNSPECIFIED);
                    } else {
                        listViewItemView.measure(View.MeasureSpec.makeMeasureSpec(listViewWidthPx,
                                View.MeasureSpec.AT_MOST), View.MeasureSpec.makeMeasureSpec(0,
                                View.MeasureSpec.UNSPECIFIED));
                    }

                    totalHght += listViewItemView.getMeasuredHeight();
                }

                ViewGroup.LayoutParams params = listView.getLayoutParams();
                // Need to manually include the height of the last divider if height of a ListView based on the height
                // of its children is set after content is set on their views.
                int divdCount = (beforeChildViewsContentSet) ? listAdpt.getCount() - 1 : listAdpt.getCount();
                params.height = totalHght + (listView.getDividerHeight() * divdCount);
            }

            /**
             * Set the height of a ListView based on the height of its children before content is set on their views.
             */
            public static void setListViewHeightBasedOnChildren(ListView listView) {
                setListViewHeightBasedOnChildren(listView, true, -1);
            }

            /**
             * Set the height of a ListView based on the height of its children after content is set on their views.
             * @param listViewWidthPx The number of pixels the width of the ListView will be
             */
            public static void setListViewHeightBasedOnChildrenAfterContent(ListView listView, int listViewWidthPx) {
                setListViewHeightBasedOnChildren(listView, false, listViewWidthPx);
            }

        }

    }

    public static class DialogUtils {

        /**
         * Dismiss a dialog if it is showing.
         */
        public static void dismissDialog(Dialog dialog) {
            if (dialog != null && dialog.isShowing()) {
                dialog.dismiss();
            }
        }

        /**
         * Helper to show a dialog after a fragment's instance state has been saved.
         */
        public static void showDialogOnSavedInstanceStateHelper(FragmentActivity activity, BaseDialog baseDialog,
                                                                boolean wasInstanceStateSaved) {
            if (wasInstanceStateSaved) {
                if (!activity.isFinishing() && !activity.isDestroyed()) {
                    activity.getSupportFragmentManager().beginTransaction()
                            .add(baseDialog.getFragment(), null)
                            .commitAllowingStateLoss();
                }
            } else {
                baseDialog.show();
            }
        }

    }

    public static class ServicesUtils {

        /**
         * Hide the soft keyboard.
         */
        public static void hideKeyboard(FragmentActivity activity) {
            View view = activity.getCurrentFocus();
            if (view != null) {
                InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        }

        /**
         * Show the soft keyboard.
         */
        public static void showKeyboard(FragmentActivity activity) {
            View view = activity.getCurrentFocus();
            if (view != null) {
                InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
            }
        }

    }

    /**
     * UI lifecycle utilities
     */
    public static class LifecycleUtils {

        /**
         * Process the up navigation event for an activity with nested fragments.
         */
        public static void processNestedFragmentActivityOnNavigateUp(FragmentActivity fragmentActivity) {
            if (fragmentActivity.getSupportFragmentManager().getBackStackEntryCount() > 0) {
                fragmentActivity.getSupportFragmentManager().popBackStack();

                UIUtils.ServicesUtils.hideKeyboard(fragmentActivity);
            } else {
                fragmentActivity.setResult(0);
                fragmentActivity.finish();
            }
        }

    }

}
