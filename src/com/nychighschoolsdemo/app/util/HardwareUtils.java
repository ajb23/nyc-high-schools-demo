package com.nychighschoolsdemo.app.util;

import android.graphics.Point;
import android.os.Build;
import android.support.v4.app.FragmentActivity;
import android.view.Display;

public class HardwareUtils {

    public static int[] getScreenWidthAndHeight(FragmentActivity activity) {
        int[] vals = new int[2];

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            Display display = activity.getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            vals[0] = size.x;
            vals[1] = size.y;
        } else {
            vals[0] = activity.getWindowManager().getDefaultDisplay().getWidth();
            vals[1] = activity.getWindowManager().getDefaultDisplay().getHeight();
        }
        return vals;
    }

}
