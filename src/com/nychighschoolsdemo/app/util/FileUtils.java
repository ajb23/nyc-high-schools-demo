package com.nychighschoolsdemo.app.util;

import android.content.Context;
import android.content.res.AssetManager;
import android.os.Environment;
import com.nychighschoolsdemo.app.base.Application;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * File utilities
 */
public class FileUtils {
    private static final Logger LOGGER = LoggerFactory.getLogger(FileUtils.class);

    /**
     * General file utilities
     */
    public static final class GeneralUtils {
        private static final SimpleDateFormat TIMESTAMPED_FILE_NAME_SIMPLE_DATE_FORMAT = new SimpleDateFormat(
                "yyyyMMdd_HHmmss", Locale.US);
        /*
        17_59_562032254675304687245
         */

        /**
         * Create a timestamped temporary file.
         * @param fileName The file name
         * @param storageDirectory The storage directory of the file
         * @return The created file
         */
        public static File createTimestampedTemporaryFile(String fileName, String fileExtension,
                                                          File storageDirectory) throws IOException {
            String timeStamp = TIMESTAMPED_FILE_NAME_SIMPLE_DATE_FORMAT.format(new Date());
            String tmstFileName = fileName + "_" + timeStamp;
            return File.createTempFile(tmstFileName, "." + fileExtension, storageDirectory);
        }

    }

    /**
     * File write utilities
     */
    public static final class WriteUtils {

        /**
         * Copy a file to an output stream.
         */
        private static void copyFileToOutputStream(InputStream in, OutputStream out) throws IOException {
            byte[] buffer = new byte[1024];
            int read;

            while((read = in.read(buffer)) != -1) {
                out.write(buffer, 0, read);
            }
        }

    }

    /**
     * File read utilities
     */
    public static final class ReadUtils {

        public static String readFile(File file) {
            StringBuilder sb = new StringBuilder(200);
            try {
                BufferedReader br = new BufferedReader(new FileReader(file));
                String line;
                while ((line = br.readLine()) != null) {
                    sb.append(line);
                    sb.append('\n');
                }
                br.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return sb.toString();
        }

    }

    /**
     * File Android utilities
     */
    public static final class AndroidUtils {

        /**
         * Copy a file from the assets directory to the application data files directory.
         */
        public static void copyFileFromAssetsDirectoryToApplicationDataFilesDirectory(String fileName) {
            AssetManager assetManager = Application.getContext().getAssets();
            try (InputStream inptStrm = assetManager.open(fileName); OutputStream out = new FileOutputStream(new File(
                    Application.getContext().getFilesDir(), fileName))) {
                WriteUtils.copyFileToOutputStream(inptStrm, out);
            } catch(IOException e) {
                LOGGER.error("Failed to copy file \"{}\" from assets: {}", fileName, e.toString());
            }
        }

        /**
         * Get the application data directory (/data/data/&lt;application package&gt;).
         */
        public static String getApplicationDataDirectory() {
            return Application.getContext().getApplicationInfo().dataDir;
        }

        /**
         * Get the application data files directory (/data/data/&lt;application package&gt;/files).
         */
        public static String getApplicationDataFilesDirectory() {
            return Application.getContext().getFilesDir().getAbsolutePath();
        }

        /**
         * Get the external storage directory (/storage/emulated/0).
         */
        public static String getExternalStorageDirectory() {
            return Environment.getExternalStorageDirectory().getAbsolutePath();
        }

        /**
         * Get the external application cache directory
         * (/storage/emulated/0/Android/data/&lt;application package&gt;/cache).
         */
        public static File getExternalCacheDirectory() {
            return Application.getContext().getExternalCacheDir();
        }

        /**
         * Get the external storage pictures directory (/storage/emulated/0/Pictures).
         */
        public static File getExternalStoragePicturesDirectory() {
            return Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        }

        /**
         * Get the external file picture directory (/storage/emulated/0/Android/data/com.fencedin.app/files/Pictures).
         * Note that this retrieves the file path specified in res/xml/file_paths.xml.
         */
        public static File getExternalFilesPictureDir(Context context) {
            return context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        }

    }

}
