package com.nychighschoolsdemo.app.util;

import java.math.RoundingMode;
import java.text.DecimalFormat;

/**
 * Number utilities
 */
public final class NumberUtils {

    /**
     * Number general utilities
     */
    public static final class GeneralUtils {

        /**
         * Check if a string is numeric.
         */
        public static boolean isNumeric(String string) {
            try {
                long l = Long.parseLong(string);
            } catch (NumberFormatException | NullPointerException e) {
                return false;
            }
            return true;
        }

    }

    /**
     * Pad a number with zeros on the left.
     * @param number The number to pad
     * @param numIntegers The number of places to pad the number
     */
    public static String padNumber(int number, int numIntegers) {
        // Manually add negative as String.format will not pad negative numbers.
        return ((number < 0) ? "-" : "") + String.format("%0" + numIntegers + "d", Math.abs(number));
    }

    /**
     * See {@link #formatDecimal(double, int, boolean, boolean)}; with no padding by default.
     */
    public static String formatDecimal(double decimal, int numPlaces, boolean round) {
        return formatDecimal(decimal, numPlaces, round, false);
    }

    /**
     * See {@link #formatDecimal(double, int, boolean, boolean)}; with rounding and no padding by default.
     */
    public static String formatDecimal(double decimal, int numPlaces) {
        return formatDecimal(decimal, numPlaces, true, false);
    }

    /**
     * See {@link #formatDecimal(double, int, boolean, boolean)}; with padding by default.
     */
    public static String formatDecimalWithPadding(double decimal, int numPlaces, boolean round) {
        return formatDecimal(decimal, numPlaces, round, true);
    }

    /**
     * See {@link #formatDecimal(double, int, boolean, boolean)}; with rounding and padding by default.
     */
    public static String formatDecimalWithPadding(double decimal, int numPlaces) {
        return formatDecimal(decimal, numPlaces, true, true);
    }

    /**
     * Format a decimal number to the given decimal of places. If the number does not have a decimal or numPlaces is set
     * to 0, it will be returned as is.
     * @param decimal The decimal to format
     * @param numPlaces The number of decimal places to format the decimal to
     * @param round Whether or not to round the decimal to the specified number of decimal places using half-up rounding
     *              mode (e.g. if using 2 decimal places, 6.375 would be rounded to 6.38 and same if negative)
     * @param withPadding Whether or not to format with padding
     */
    public static String formatDecimal(double decimal, int numPlaces, boolean round, boolean withPadding) {
        String frmtChar = (withPadding) ? "0" : "#";
        String repeated = (numPlaces > 0) ? "." + new String(new char[numPlaces]).replace("\0", frmtChar): "";
        DecimalFormat decimalFormat = new DecimalFormat("#,###" + repeated);

        if (decimal >= 0) {
            RoundingMode roundingMode = (round) ? RoundingMode.HALF_UP : RoundingMode.FLOOR;
            decimalFormat.setRoundingMode(roundingMode);
        } else {
            RoundingMode roundingMode = (round) ? RoundingMode.HALF_DOWN : RoundingMode.CEILING;
            decimalFormat.setRoundingMode(roundingMode);
        }
        return decimalFormat.format(decimal);
    }

    /**
     * Format a decimal number to be exactly the given number of decimal places. If the given number of decimal places
     * is less than the given decimal, the decimal will be truncated and rounded.
     */
    public static String formatWithDecimalPadded(double decimal, int numPlaces) {
        String repeated = (numPlaces > 0) ? "." + new String(new char[numPlaces]).replace("\0", "0") : "";
        DecimalFormat decimalFormat = new DecimalFormat("#,##0" + repeated);
        return decimalFormat.format(decimal);
    }

    /**
     * Number round utilities
     */
    public static final class RoundUtils {

        /**
         * Rounding using half-up mode.
         */
        public static int roundHalfUp(double decimal) {
            return (int) Math.round(decimal);
        }

        /**
         * Rounding using half-up mode with decimal precision.
         */
        public static double roundUsingHalfUpMode(double decimal, int numPlaces) {
            double numPlacesFinal = Math.pow(10, numPlaces);
            return (double) Math.round(decimal * numPlacesFinal) / numPlacesFinal;
        }

    }

}
