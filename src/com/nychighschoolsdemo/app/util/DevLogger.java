package com.nychighschoolsdemo.app.util;

import java.util.HashMap;
import java.util.Map;

/**
 * A development logger
 */
public class DevLogger {
    private static int sBreakpoint;

    /**
     * Create the objects string.
     */
    private static String createObjectsString(Object... objects) {
        StringBuilder strnBuld = new StringBuilder();
        for (int i = 0; i < objects.length; i++) {
            Object obje = objects[i];
            if (obje instanceof Map) {
                strnBuld.append("Map:\n");
                for (Object entry : ((HashMap) obje).entrySet()) {
                    strnBuld.append("  ").append(entry.toString()).append("\n");
                }
            } else {
                strnBuld.append(obje).append("\n");
            }
        }
        if (strnBuld.length() > 0) {
            strnBuld.setLength(strnBuld.length() - 1);
        }
        return strnBuld.toString();
    }

    /**
     * Print the given output within the development log top and bottom separators.
     */
    public static void printGivenOutputWithinTopAndBottomSeparators(Object object) {
        System.out.println("\n==============  DEV LOG  ==============");
        System.out.println((object == null) ? "null" : object);
        System.out.println("==============  ! DEV LOG  ==============");
    }

    /**
     * Log output.
     */
    public static void log(Object... objects) {
        printGivenOutputWithinTopAndBottomSeparators(createObjectsString(objects));
    }

    /**
     * Log output with no header.
     */
    public static void logNoHeader(Object... objects) {
        System.out.println(createObjectsString(objects));
    }

    /**
     * Print a sequential breakpoint.
     */
    public static void printSequentialBreakpoint() {
        sBreakpoint++;
        System.out.println("\n========== BREAKPOINT: " + sBreakpoint + " ==========");
    }

    /**
     * Print a breakpoint.
     */
    public static void printBreakpoint(int num) {
        System.out.println("\n========== BREAKPOINT: " + num + " ==========");
    }

    /**
     * Print the byte string representation of one or more bytes.
     */
    public static void byteStrings(byte[] bytes) {
        StringBuilder strnBuld = new StringBuilder();

        for (int i = 0; i < bytes.length; i++) {
            strnBuld.append(i).append(": ").append(Integer.toBinaryString(bytes[i])).append("\n");
        }

        printGivenOutputWithinTopAndBottomSeparators(strnBuld);
    }

}
