package com.nychighschoolsdemo.app.util;

import android.util.Base64;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringUtils {

    public static class Encode {

        public static String encodeBase64(byte[] bytes) {
            return Base64.encodeToString(bytes, Base64.NO_WRAP);
        }

        public static byte[] decodeBase64(String encoded) {
            return Base64.decode(encoded, Base64.NO_WRAP);
        }

    }

    public static class Formatting {

        private static String formatTextAsId(String text, String allowedCharactersRegex, int maxLength) {
            text = text.toLowerCase();
            Matcher mtch = Pattern.compile(allowedCharactersRegex).matcher(text);
            StringBuilder idStrnBldr = new StringBuilder(100);
            while (mtch.find()) {
                String group = mtch.group(0);
                if (!group.equals("")) {
                    idStrnBldr.append(group);
                    idStrnBldr.append("-");
                }
            }
            if (idStrnBldr.length() > 0) {
                idStrnBldr.deleteCharAt(idStrnBldr.length() - 1);
            }
            if (idStrnBldr.length() > maxLength) {
                idStrnBldr.delete(maxLength, idStrnBldr.length());
            }
            return idStrnBldr.toString();
        }

    }

    /**
     * String search utilities
     */
    public static final class SearchUtils {

        /**
         * Get the index of the first word in a string.
         * @return The index of the first word in the string or -1 if no word exists
         */
        public static int getIndexOfFirstWord(String string) {
            if (string.length() > 0) {
                char[] strnChars = string.toCharArray();
                for (int i = 0; i < strnChars.length; i++) {
                    if (strnChars[i] != ' ') {
                        return i;
                    } else {
                        if (i == strnChars.length - 1) {
                            return -1;
                        }
                    }
                }
            }
            return -1;
        }

        /**
         * Get the index of the last word in a string.
         * @return The index of the last word in the string or -1 if no word exists
         */
        public static int getIndexOfLastWord(String string) {
            int index = -1;
            if (string.length() > 0) {
                if (string.charAt(string.length() - 1) == ' ') {
                    if (string.length() == 1) {
                        return -1;
                    }
                    boolean nonSpaceCharFound = false;
                    for (int i = string.length() - 2; i >= 0; i--) {
                        index = i;
                        if (string.charAt(i) == ' ') {
                            if (nonSpaceCharFound) {
                                index++;
                                break;
                            } else if (i == 0) {
                                index = -1;
                            }
                        } else {
                            nonSpaceCharFound = true;
                        }
                    }
                } else {
                    int lastWhtspcIndex = string.lastIndexOf(" ");
                    // The index will be -1 if no whitespace exists.
                    index = (lastWhtspcIndex == -1) ? 0 : lastWhtspcIndex + 1;
                }
            }
            return index;
        }

    }

    /**
     * String format utilities
     */
    public static final class FormatUtils {

        /**
         * See {@link #capitalizeWords(String, boolean, String[], boolean)}.
         * @param discludedWords Words to disclude
         */
        public static String capitalizeWords(String string, String[] discludedWords) {
            return capitalizeWords(string, false, discludedWords, false);
        }


        /**
         * Capitalize all the words of a String.
         * @param lowercaseLetters Set all non-first letters of each word to lowercase
         * @param discludedWords Words to disclude
         * @param forceLowercaseDiscludedWords Force lowercase the letters of all discluded words in the string
         */
        public static String capitalizeWords(String string, boolean lowercaseLetters, String[] discludedWords,
                                             boolean forceLowercaseDiscludedWords) {
            String words[] = string.split("\\s");
            StringBuilder captdWords = new StringBuilder(string.length());
            for (String word : words) {
                // The word will be empty for each extra space that exists (except at the end) and also if the text is
                // empty itself.
                if (word.equals("")) {
                    captdWords.append(" ");
                    continue;
                }
                boolean isWordDiscd = false;
                if (discludedWords != null) {
                    for (String discWord : discludedWords) {
                        if (discWord.toLowerCase().equals(word.toLowerCase())) {
                            isWordDiscd = true;
                        }
                    }
                }
                if (isWordDiscd) {
                    if (forceLowercaseDiscludedWords) {
                        captdWords.append(word.toLowerCase());
                    } else {
                        captdWords.append(word);
                    }
                } else {
                    String remnLetts = (lowercaseLetters) ? word.substring(1).toLowerCase() : word.substring(1);
                    captdWords.append(Character.toUpperCase(word.charAt(0))).append(remnLetts);
                }
                captdWords.append(" ");
            }
            if (captdWords.length() > 0) {
                captdWords.deleteCharAt(captdWords.length() - 1);
            }
            if (string.length() > 0) {
                if (string.charAt(string.length() - 1) == ' ') {
                    captdWords.append(" ");
                    for (int i = string.length() - 2; i >= 0; i--) {
                        if (string.charAt(i) != ' ') {
                            break;
                        } else {
                            captdWords.append(" ");
                        }
                    }
                }
            }
            return captdWords.toString();
        }

        /**
         * Format a parameter string by replacing each occurrence of "{}" with its corresponding argument.
         * @param s The base string
         * @param objects The string arguments
         * @return The formatted string
         */
        public static String formatParameterString(String s, Object... objects) {
            for (Object object : objects) {
                // Note: Character "$" is the regex group reference character and must be escaped in the object string to
                // prevent #replaceFirst from throwing error "java.lang.IllegalArgumentException: Illegal group reference".
                String objcStrn = object.toString();
                objcStrn = objcStrn.replaceAll("\\$", Matcher.quoteReplacement("\\$"));
                s = s.replaceFirst("\\{\\}", objcStrn);
            }
            return s;
        }

        /**
         * Capitalize the first letter of each string in the array.
         * @return Array of strings with first letter uppercase.
         */
        public static String[] capitalizeStringsFirstLetter(String[] strings) {
            for (int i = 0; i < strings.length; i++) {
                if (strings[i] != null && strings[i].length() > 0) {
                    String frstLttr = strings[i].substring(0, 1).toUpperCase();
                    strings[i] = strings[i].length() > 1 ? frstLttr + strings[i].substring(1) : frstLttr;
                }

            }
            return strings;
        }

    }

}
