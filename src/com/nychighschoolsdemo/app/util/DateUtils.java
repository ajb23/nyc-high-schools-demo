package com.nychighschoolsdemo.app.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.DateFormat;
import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Date utilities
 */
public class DateUtils {
    private static final Logger LOGGER = LoggerFactory.getLogger(DateUtils.class);
    private static final DateFormat ISO8601_DATE_FORMAT;
    private static final DateFormat ISO8601_DATE_FORMAT_NO_MILLISECONDS;
    private static final DateFormat ISO8601_DATE_FORMAT_UTC;
    private static final DateFormat ISO8601_DATE_FORMAT_NO_MILLISECONDS_UTC;
    private static final DateFormat ISO8601_DATE_FORMAT_WITH_TIMEZONE_UTC;
    private static final List<String> ISO_8601_STRING_LIST = Arrays.asList(
            "yyyy-MM-dd'T'HH:mm:ss'Z'",
            "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'",
            "yyyy-MM-dd'T'HH:mm:ssZ",
            "yyyy-MM-dd'T'HH:mm:ss.SSSZ",
            "yyyy-MM-dd'T'HH:mm:ssXXX",
            "yyyy-MM-dd'T'HH:mm:ss.SSSXXX",
            "yyyy-MM-dd HH:mm:ss",
            "yyyy-MM-dd HH:mm:ss.SSS",
            "yyyy-MM-dd HH:mm:ss Z",
            "yyyy-MM-dd HH:mm:ss.SSS Z",
            "yyyy-MM-dd HH:mm:ss XXX",
            "yyyy-MM-dd HH:mm:ss.SSS XXX");
    public static final Calendar UTC_CALENDAR;

    static {
        ISO8601_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US);
        ISO8601_DATE_FORMAT_NO_MILLISECONDS = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.US);
        ISO8601_DATE_FORMAT_UTC = (DateFormat) ISO8601_DATE_FORMAT.clone();
        ISO8601_DATE_FORMAT_NO_MILLISECONDS_UTC = (DateFormat) ISO8601_DATE_FORMAT_NO_MILLISECONDS.clone();
        ISO8601_DATE_FORMAT_WITH_TIMEZONE_UTC = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX", Locale.US);

        TimeZone timeZoneUtc = TimeZone.getTimeZone("UTC");
        ISO8601_DATE_FORMAT_UTC.setTimeZone(timeZoneUtc);
        ISO8601_DATE_FORMAT_NO_MILLISECONDS_UTC.setTimeZone(timeZoneUtc);
        ISO8601_DATE_FORMAT_WITH_TIMEZONE_UTC.setTimeZone(timeZoneUtc);

        UTC_CALENDAR = Calendar.getInstance();
        UTC_CALENDAR.setTimeZone(timeZoneUtc);
    }

    /**
     * Convert date to ISO-8601 format (yyyy-MM-dd'T'HH:mm:ss.SSS) using UTC time by default.
     * See {@link #toIso8601(Date, boolean)}.
     * @param date Date to format
     * @return Date formatted in ISO-8601.
     */
    public static String toIso8601(Date date) {
        return toIso8601(date, true);
    }

    /**
     * Convert date to ISO-8601 format (yyyy-MM-dd'T'HH:mm:ss.SSS).
     * @param date Date to format
     * @param formatToUtcTimeZone Whether or not to format to UTC time
     * @return Date formatted in ISO-8601.
     */
    public static String toIso8601(Date date, boolean formatToUtcTimeZone) {
        if (formatToUtcTimeZone) {
            return ISO8601_DATE_FORMAT_UTC.format(date);
        } else {
            return ISO8601_DATE_FORMAT.format(date);
        }
    }

    /**
     * Convert date to ISO-8601 format discluding milliseconds (yyyy-MM-dd'T'HH:mm:ss) using UTC time by default.
     * See {@link #toIso8601NoMillis(Date, boolean)}.
     * @param date Date to format
     * @return Date formatted in ISO-8601 discluding milliseconds
     */
    public static String toIso8601NoMillis(Date date) {
        return toIso8601NoMillis(date, true);
    }

    /**
     * Convert date to ISO-8601 format discluding milliseconds (yyyy-MM-dd'T'HH:mm:ss).
     * @param date Date to format
     * @param formatToUtcTimeZone Whether or not to format to UTC time
     * @return Date formatted in ISO-8601 discluding milliseconds
     */
    public static String toIso8601NoMillis(Date date, boolean formatToUtcTimeZone) {
        if (formatToUtcTimeZone) {
            return ISO8601_DATE_FORMAT_NO_MILLISECONDS_UTC.format(date);
        } else {
            return ISO8601_DATE_FORMAT_NO_MILLISECONDS.format(date);
        }
    }

    /**
     * Convert date to ISO-8601 format with a timezone offset (yyyy-MM-dd'T'HH:mm:ss.SSS+-00).
     * @param date Date to format
     * @return Date formatted in ISO-8601.
     */
//    public static String toIso8601WithTimezoneOffset(Date date, int timezoneOffset) {
//        DateFormat ISO8601_DATE_FORMAT_WITH_TIMEZONE = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX", Locale.US);
//
//        TimeZone timeZone = TimeZone.getTimeZone("UTC");
//        ISO8601_DATE_FORMAT.setTimeZone(timeZone);
//        return ISO8601_DATE_FORMAT.format(date);
//    }

    /**
     * Parse a ISO-8601 formatted date string to a Date object using UTC time by default.
     * See {@link #fromIso8601(String, boolean)}.
     */
    public static Date fromIso8601(String iso8601Date) {
        return fromIso8601(iso8601Date, true);
    }

    /**
     * Parse a ISO-8601 formatted date string to a Date object.
     * @param iso8601Date ISO-8601 formatted date string
     * @param formatFromUtcTimeZone Whether or not to format from UTC time
     * @return Date object of ISO-8601 date.
     */
    public static Date fromIso8601(String iso8601Date, boolean formatFromUtcTimeZone) {
        Date date = null;
        for (String is08601Strn : ISO_8601_STRING_LIST) {
            try {
                DateFormat dateForm = new SimpleDateFormat(is08601Strn, Locale.US);
                if (formatFromUtcTimeZone) {
                    dateForm.setTimeZone(TimeZone.getTimeZone("UTC"));
                }
                date = dateForm.parse(iso8601Date);
                break;
            } catch (ParseException ignored) { }
        }

        if (date == null) {
            LOGGER.error("Failed to parse ISO-8601 string \"{}\" to Date", iso8601Date);
        }
        return date;
    }

    /**
     * Date conversion utilities
     */
    public static final class ConversionUtils {

        /**
         * Convert milliseconds to seconds.
         */
        public static double millisecondsToSeconds(long milliseconds) {
            return milliseconds / 1000.0;
        }

        /**
         * Convert milliseconds to minutes.
         */
        public static double millisecondsToMinutes(long milliseconds) {
            return milliseconds / 1000.0 / 60.0;
        }

        /**
         * Convert milliseconds to days.
         */
        public static double millisecondsToDays(long milliseconds) {
            return (double) milliseconds / ( 1000 * 60 * 60 * 24 );
        }

        /**
         * Convert seconds to minutes.
         */
        public static double secondsToMinutes(int seconds) {
            return seconds / 60.0;
        }

        /**
         * Convert minutes to seconds.
         */
        public static int minutesToSeconds(int minutes) {
            return minutes * 60;
        }

        /**
         * Convert minutes to hours.
         */
        public static double minutesToHours(int minutes) {
            return minutes / 60.0;
        }

        /**
         * Convert minutes to milliseconds.
         */
        public static long minutesToMilliseconds(int minutes) {
            return minutes * ( 1000 * 60 );
        }

        /**
         * Convert hours to milliseconds.
         */
        public static long hoursToMilliseconds(int hours) {
            return hours * ( 1000 * 60 * 60 );
        }

        /**
         * Convert hours to minutes.
         */
        public static int hoursToMinutes(int hours) {
            return hours * 60;
        }

    }

    /**
     * Convert milliseconds to seconds as a formatted String. If the number is a whole number, no decimal places will be
     * included. If not, at most the specified number of decimal places will be used.
     */
    public static String formatMillisecondsToSeconds(long milliseconds, int decimalPlaces) {
        return NumberUtils.formatDecimal((double) milliseconds / 1000, decimalPlaces);
    }

    /**
     * Same as formatMillisecondsToSeconds(long, int) using no decimal places by default.
     */
    public static String formatMillisecondsToSeconds(long milliseconds) {
        return NumberUtils.formatDecimal((double) milliseconds / 1000, 0);
    }

    /**
     * Convert milliseconds to minutes as a formatted String. If the number is a whole number, no decimal places will be
     * included. If not, at most the specified number of decimal places will be used.
     */
    public static String formatMillisecondsToMinutes(long milliseconds, int decimalPlaces) {
        return NumberUtils.formatDecimal((double) milliseconds / (1000 * 60), decimalPlaces);
    }

    /**
     * Same as formatMillisecondsToMinutes(long, int) using no decimal places by default.
     */
    public static String formatMillisecondsToMinutes(long milliseconds) {
        return NumberUtils.formatDecimal((double) milliseconds / (1000 * 60), 0);
    }

    /**
     * Convert milliseconds to hours as a formatted String. If the number is a whole number, no decimal places will be
     * included. If not, at most the specified number of decimal places will be used.
     */
    public static String formatMillisecondsToHours(long milliseconds, int decimalPlaces) {
        return NumberUtils.formatDecimal((double) milliseconds / (1000 * 60 * 60), decimalPlaces);
    }

    /**
     * Same as formatMillisecondsToHours(long, int) using no decimal places by default.
     */
    public static String formatMillisecondsToHours(long milliseconds) {
        return NumberUtils.formatDecimal((double) milliseconds / (1000 * 60 * 60), 0);
    }

    /**
     * Convert milliseconds to days as a formatted String. If the number of days is a whole number, no decimal places
     * will be included. If not, at most the specified number of decimal places will be used.
     */
    public static String formatMillisecondsToDays(long milliseconds, int decimalPlaces) {
        return NumberUtils.formatDecimal(ConversionUtils.millisecondsToDays(milliseconds), decimalPlaces);
    }

    /**
     * Same as formatMillisecondsToDays(long, int) using no decimal places by default.
     */
    public static String formatMillisecondsToDays(long milliseconds) {
        return NumberUtils.formatDecimal((double) milliseconds / (1000 * 60 * 60 * 24), 0);
    }

    /**
     * Format milliseconds to a time measured with the correct unit.
     * @return A time with its unit of measurement (e.g. "42 minutes")
     */
    public static String formatMillisecondsToTimeWithUnit(long milliseconds) {
        String unit, remTimeStr;
        if (milliseconds < 1000 * 60) {
            remTimeStr = (milliseconds >= 500) ? DateUtils.formatMillisecondsToSeconds(milliseconds) : "1";
            unit = (milliseconds >= 1000 * 1.5) ? " seconds" : " second";
        } else if (milliseconds < ( 1000 * 60 ) * 59) { // 1000 * 60 = 1 minute
            remTimeStr = DateUtils.formatMillisecondsToMinutes(milliseconds);
            unit = (milliseconds >= 1000 * 60 * 1.5) ? " minutes" : " minute";
        } else if (milliseconds <= ( 1000 * 60 * 60 ) * 23) { // 1000 * 60 * 60 = 1 hour
            remTimeStr = DateUtils.formatMillisecondsToHours(milliseconds);
            unit = (milliseconds >= 1000 * 60 * 60 * 1.5) ? " hours" : " hour";
        } else {
            remTimeStr = DateUtils.formatMillisecondsToDays(milliseconds);
            unit = (milliseconds >= 1000 * 60 * 60 * 24 * 1.5) ? " days" : " day";
        }
        return remTimeStr + unit;
    }

    /**
     * Format milliseconds to a time measured with the correct unit in the short format (e.g. "1 day" to "1d")
     * @return A time with its unit of measurement (e.g. "42m")
     */
    public static String formatMillisecondsToShortFormatTimeWithUnit(long milliseconds) {
        String unit;
        String remTimeStr;
        if (milliseconds < 1000 * 60) {
            remTimeStr = "<1";
            unit = "m";
        } else if (milliseconds < ( 1000 * 60 ) * 59) { // 1000 * 60 = 1 minute
            remTimeStr = DateUtils.formatMillisecondsToMinutes(milliseconds);
            unit = "m";
        } else if (milliseconds <= ( 1000 * 60 * 60 ) * 23) { // 1000 * 60 * 60 = 1 hour
            remTimeStr = DateUtils.formatMillisecondsToHours(milliseconds);
            unit = "h";
        } else {
            remTimeStr = DateUtils.formatMillisecondsToDays(milliseconds);
            unit = "d";
        }
        return remTimeStr + unit;
    }

    /**
     * Get a new {@link java.util.Calendar} instance using the UTC time zone.
     */
    public static Calendar getUTCCalendar() {
        return UTC_CALENDAR;
    }

    /**
     * Convert a year, month of year and day of month to a date.
     */
    public static Date convertYearAndMonthOfYearAndDayOfMonthToDate(int year, int monthOfYear, int dayOfMonth) {
        Calendar caln = Calendar.getInstance();
        caln.set(Calendar.YEAR, year);
        caln.set(Calendar.MONTH, monthOfYear);
        caln.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        caln.set(Calendar.MILLISECOND, 0);
        caln.set(Calendar.SECOND, 0);
        caln.set(Calendar.MINUTE, 0);
        caln.set(Calendar.HOUR_OF_DAY, 0);
        return caln.getTime();
    }

    /**
     * Date round utilities
     */
    public static final class RoundUtils {

        /**
         * Round a date to a time unit using half-up rounding mode.
         */
        public static Date roundDateHalfUpToTimeUnit(Date date, int calendarUnit) {
            return roundDateHalfUpToTimeUnit(date, calendarUnit, false);
        }

        /**
         * Round a date to a time unit using half-up rounding mode.
         * @param roundUtcTime Round the UTC time of the date
         * @return Rounded date of the current time zone or UTC time
         */
        public static Date roundDateHalfUpToTimeUnit(Date date, int calendarUnit, boolean roundUtcTime) {
            Calendar caln;
            if (roundUtcTime) {
                caln = DateUtils.getUTCCalendar();
            } else {
                caln = Calendar.getInstance();
            }
            caln.setTime(date);
            switch (calendarUnit) {
                case Calendar.SECOND:
                    caln.add(Calendar.MILLISECOND, 500);
                    caln.set(Calendar.MILLISECOND, 0);
                    break;
                case Calendar.MINUTE:
                    caln.set(Calendar.MILLISECOND, 0);
                    caln.add(Calendar.SECOND, 30);
                    caln.set(Calendar.SECOND, 0);
                    break;
                case Calendar.HOUR:
                    caln.set(Calendar.MILLISECOND, 0);
                    caln.set(Calendar.SECOND, 0);
                    caln.add(Calendar.MINUTE, 30);
                    caln.set(Calendar.MINUTE, 0);
                    break;
                case Calendar.DAY_OF_MONTH:
                    caln.set(Calendar.MILLISECOND, 0);
                    caln.set(Calendar.SECOND, 0);
                    caln.set(Calendar.MINUTE, 0);
                    caln.add(Calendar.HOUR_OF_DAY, 12);
                    caln.set(Calendar.HOUR_OF_DAY, 0);
                    break;
                case Calendar.MONTH:
                    caln.set(Calendar.MILLISECOND, 0);
                    caln.set(Calendar.SECOND, 0);
                    caln.set(Calendar.MINUTE, 0);
                    caln.set(Calendar.HOUR_OF_DAY, 0);
                    int monthMaxDays = caln.getActualMaximum(Calendar.DAY_OF_MONTH);
                    int monthMiddDay = NumberUtils.RoundUtils.roundHalfUp(monthMaxDays / 2.0);
                    int currDayOfMonth = caln.get(Calendar.DAY_OF_MONTH);
                    if (currDayOfMonth >= monthMiddDay) {
                        // Note that when the current month has more days than the next and the month day of the current
                        // date is a day that does not exist in the next month and the month is increased by one, the day of
                        // the current date will be set to the last day of the next month.
                        caln.add(Calendar.MONTH, 1);
                    }
                    caln.set(Calendar.DAY_OF_MONTH, 1);
                    break;
                case Calendar.YEAR:
                    caln.set(Calendar.MILLISECOND, 0);
                    caln.set(Calendar.SECOND, 0);
                    caln.set(Calendar.MINUTE, 0);
                    caln.set(Calendar.HOUR_OF_DAY, 0);
                    int yearMiddDay = NumberUtils.RoundUtils.roundHalfUp(365 / 2.0);
                    int currDayOfYear = caln.get(Calendar.DAY_OF_YEAR);
                    if (currDayOfYear >= yearMiddDay) {
                        caln.add(Calendar.YEAR, 1);
                    }
                    caln.set(Calendar.DAY_OF_MONTH, 1);
                    caln.set(Calendar.MONTH, 0);
                    break;
                default:
                    throw new IllegalArgumentException("Invalid calendar unit");
            }
            return caln.getTime();
        }

        /**
         * Floor round a date to a time unit.
         */
        public static Date floorRoundDateToTimeUnit(Date date, int calendarUnit) {
            return floorRoundDateToTimeUnit(date, calendarUnit, false);
        }

        /**
         * Floor round a date to a time unit.
         * @param roundUtcTime Round the UTC time of the date
         * @return Rounded date of the current time zone or UTC time
         */
        public static Date floorRoundDateToTimeUnit(Date date, int calendarUnit, boolean roundUtcTime) {
            Calendar caln;
            if (roundUtcTime) {
                caln = DateUtils.getUTCCalendar();
            } else {
                caln = Calendar.getInstance();
            }
            caln.setTime(date);
            switch (calendarUnit) {
                case Calendar.SECOND:
                    caln.set(Calendar.MILLISECOND, 0);
                    break;
                case Calendar.MINUTE:
                    caln.set(Calendar.MILLISECOND, 0);
                    caln.set(Calendar.SECOND, 0);
                    break;
                case Calendar.HOUR:
                    caln.set(Calendar.MILLISECOND, 0);
                    caln.set(Calendar.SECOND, 0);
                    caln.set(Calendar.MINUTE, 0);
                    break;
                case Calendar.DAY_OF_MONTH:
                    caln.set(Calendar.MILLISECOND, 0);
                    caln.set(Calendar.SECOND, 0);
                    caln.set(Calendar.MINUTE, 0);
                    // Note that even though Calendar uses time zone UTC by default, #set() runs on the current time
                    // zone for time unit hour. Also note that time unit HOUR uses hours 0 - 11 and HOUR_OF_DAY uses
                    // hours 0 - 23.
                    caln.set(Calendar.HOUR_OF_DAY, 0);
                    break;
                case Calendar.MONTH:
                    caln.setTime(floorRoundDateToTimeUnit(caln.getTime(), Calendar.DAY_OF_MONTH, roundUtcTime));
                    caln.set(Calendar.DAY_OF_MONTH, 0);
                    // Note that setting the day of month to 0 sets it to the last day of the previous month so one day has
                    // to be added.
                    caln.add(Calendar.DAY_OF_MONTH, 1);
                    break;
                case Calendar.YEAR:
                    caln.setTime(floorRoundDateToTimeUnit(caln.getTime(), Calendar.MONDAY, roundUtcTime));
                    caln.set(Calendar.MONTH, 0);
                    break;
                default:
                    throw new IllegalArgumentException("Invalid calendar unit");
            }
            return caln.getTime();
        }

        /**
         * Ceiling round a date to a time unit.
         */
        public static Date ceilingRoundDateToTimeUnit(Date date, int calendarUnit) {
            return ceilingRoundDateToTimeUnit(date, calendarUnit, false);
        }

        /**
         * Ceiling round a date to a time unit.
         * @param roundUtcTime Round the UTC time of the date
         * @return Rounded date of the current time zone or UTC time
         */
        public static Date ceilingRoundDateToTimeUnit(Date date, int calendarUnit, boolean roundUtcTime) {
            Calendar caln;
            if (roundUtcTime) {
                caln = DateUtils.getUTCCalendar();
            } else {
                caln = Calendar.getInstance();
            }
            caln.setTime(floorRoundDateToTimeUnit(date, calendarUnit, roundUtcTime));
            switch (calendarUnit) {
                case Calendar.SECOND:
                    caln.add(Calendar.SECOND, 1);
                    break;
                case Calendar.MINUTE:
                    caln.add(Calendar.MINUTE, 1);
                    break;
                case Calendar.HOUR:
                    caln.add(Calendar.HOUR, 1);
                    break;
                case Calendar.DAY_OF_MONTH:
                    caln.add(Calendar.DAY_OF_MONTH, 1);
                    break;
                case Calendar.MONTH:
                    caln.add(Calendar.MONTH, 1);
                    break;
                case Calendar.YEAR:
                    caln.add(Calendar.YEAR, 1);
                    break;
                default:
                    throw new IllegalArgumentException("Invalid calendar unit");
            }
            return caln.getTime();
        }

    }

    /**
     * Date format utilities
     */
    public static final class FormatUtils {

        /**
         * Convert a time offset in seconds to a time offset string in the format [+-][0-9]{4}.
         */
        public static String convertTimeOffsetSecondsToTimeOffsetString(int timeOffsetSeconds) {
            int timeOffsMints = (int) DateUtils.ConversionUtils.secondsToMinutes(timeOffsetSeconds);
            if (timeOffsetSeconds == 0) {
                return "Z";
            } else {
                double hours = DateUtils.ConversionUtils.minutesToHours(timeOffsMints);
                int remnMints = timeOffsMints - ((int) hours * 60);
                String hoursPadd = NumberUtils.padNumber((int) hours, 2);
                String remnMintsPadd = NumberUtils.padNumber(Math.abs(remnMints), 2);
                hoursPadd = (hoursPadd.charAt(0) != '-') ? "+" + hoursPadd : hoursPadd;
                return hoursPadd + remnMintsPadd;
            }
        }

        /**
         * Format a date to a local date with a time offset in ISO-8601 format.
         */
        public static String convertDateToLocalDateWithTimeOffset(Date date, int timeOffsetSeconds) {
            Calendar localDateCalnl = Calendar.getInstance();
            localDateCalnl.setTime(date);
            localDateCalnl.add(Calendar.SECOND, timeOffsetSeconds);
            String localDateIso8601 = DateUtils.toIso8601(localDateCalnl.getTime(), false);
            String frmtdTimeOffs = convertTimeOffsetSecondsToTimeOffsetString(timeOffsetSeconds);
            return localDateIso8601.substring(0, localDateIso8601.length() - 1) + frmtdTimeOffs;
        }

    }

    /**
     * Date parse utilities
     */
    public static final class ParseUtils {

        /**
         * Extract the time offset from an ISO-8601 string.
         */
        public static String extractIso8601TimeOffsetString(String iso8601) {
            String timeOffsStrn = null;
            boolean doesTimsStrnContNegtTimeOffs = iso8601.substring(10).contains("-");
            if (iso8601.contains("+")) {
                timeOffsStrn = iso8601.substring(iso8601.lastIndexOf("+"));
            } else if (doesTimsStrnContNegtTimeOffs) {
                timeOffsStrn = iso8601.substring(iso8601.lastIndexOf("-"));
            }
            return timeOffsStrn;
        }

        /**
         * Parse a time offset string in the format [+-][0-9]{4} to hours and minutes.
         * @return An int array where the first element is hours and the second is minutes
         */
        public static int[] parseTimeOffsetToHoursAndMinutes(String timeOffset) {
            boolean doesTimsStrnContNegtTimeOffs = timeOffset.contains("-");
            int timeOffsHours = Integer.parseInt(timeOffset.substring(1, 3));
            int timeOffsMints = Integer.parseInt(timeOffset.substring(3));
            timeOffsHours = (doesTimsStrnContNegtTimeOffs) ? -1 * timeOffsHours : timeOffsHours;
            timeOffsMints = (doesTimsStrnContNegtTimeOffs) ? -1 * timeOffsMints : timeOffsMints;
            return new int[] { timeOffsHours, timeOffsMints };
        }

        /**
         * Extract the time offset from an ISO-8601 string with a time offset in the format [+-][0-9]{4}.
         * @return An int array where the first element is hours and the second is minutes; null if the ISO-8601 string
         *         did not contain a time offset
         */
        public static int[] extractIso8601TimeOffset(String iso8601) {
            String timeOffsStrn = DateUtils.ParseUtils.extractIso8601TimeOffsetString(iso8601);
            if (timeOffsStrn == null) {
                return null;
            } else {
                return DateUtils.ParseUtils.parseTimeOffsetToHoursAndMinutes(timeOffsStrn);
            }
        }

    }

    /**
     * Date UI utilities
     */
    public static final class UIUtils {

        /**
         * Convert the output of the AM/PM date symbol of a SimpleDateFormat to lowercase.
         */
        public static DateFormat convertSimpleDateFormatAmPmDateSymbolOutputToLowercase(
                SimpleDateFormat simpleDateFormat) {
            DateFormatSymbols dateFormSymbs = new DateFormatSymbols(Locale.getDefault());
            dateFormSymbs.setAmPmStrings(new String[] { "am", "pm" });
            simpleDateFormat.setDateFormatSymbols(dateFormSymbs);
            return simpleDateFormat;
        }

    }

    /**
     * Date miscellaneous utilities
     */
    public static final class MiscellaneousUtils {

        /**
         * Get the last bimonthly period month (odd number month [i.e. January, March, May, etc.]) start date.
         */
        public static Date getLastBimonthlyPeriodMonthStartDate() {
            Calendar caln = DateUtils.getUTCCalendar();
            caln.setTimeInMillis(System.currentTimeMillis());
            caln.set(Calendar.MONTH, 4);
            int month = caln.get(Calendar.MONTH);
            // Note that Calendar starts months at 0.
            if (month % 2 == 1) {
                caln.add(Calendar.MONTH, -1);
            }
            return RoundUtils.floorRoundDateToTimeUnit(caln.getTime(), Calendar.MONTH);
        }

    }

}
