package com.nychighschoolsdemo.app.util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.*;

/**
 * JSON utilities
 */
public class JsonUtils {

    /**
     * JSON sort utilities
     */
    public static final class SortUtils {

        /**
         * Sort a JSON array by the value of a key in the JSON objects (in lexicographic order; JSON objects with a null
         * node value will be placed at the end).
         * @param jsonArray The JSON array to sort
         * @param key The key to sort the JSON array by
         * @param descendingOrder Whether or not the JSON objects should be sorted in descending order
         * @return The sorted JSON array
         * @throws JSONException If the JSON array contains anything other than a JSON objects
         * @throws IllegalArgumentException If the node of the given key does not exist in one of the JSON objects
         */
        public static JSONArray sortJSONArrayByKey(JSONArray jsonArray, final String key,
                                                   final boolean descendingOrder)
                                                   throws JSONException, IllegalArgumentException {
            JSONArray sortedJsonArray = new JSONArray();
            List<JSONObject> jsonObjeList = new ArrayList<>(jsonArray.length());
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObje = jsonArray.getJSONObject(i);
                if (!jsonObje.has(key)) {
                    throw new IllegalArgumentException("A JSON object in the JSON array does not contain a node with" +
                            " key \"" + key + "\".");
                }
                jsonObjeList.add(jsonObje);
            }
            Collections.sort(jsonObjeList, new Comparator<JSONObject>() {
                @Override
                public int compare(JSONObject jsonObject1, JSONObject jsonObject2) {
                    boolean isJsonObje1NodeNull = jsonObject1.isNull(key);
                    boolean isJsonObje2NodeNull = jsonObject2.isNull(key);
                    int compResl = 0;
                    if (isJsonObje1NodeNull && !isJsonObje2NodeNull) {
                        compResl = 1;
                    } else if (!isJsonObje1NodeNull && isJsonObje2NodeNull) {
                        compResl = -1;
                    } else if (isJsonObje1NodeNull) {
                        compResl = 0;
                    } else {
                        try {
                            compResl = LangUtils.CompareUtils.compareObjects(jsonObject1.get(key), jsonObject2.get(key));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    return (descendingOrder) ? -compResl : compResl;
                }
            });

            for (int i = 0; i < jsonArray.length(); i++) {
                sortedJsonArray.put(jsonObjeList.get(i));
            }
            return sortedJsonArray;
        }

    }

    /**
     * JSON extract utilities
     */
    public static final class ExtractUtils {

        /**
         * Extract the values of a node from all objects of a JSON array.
         * @param jsonArray The JSON array of objects
         * @param key The key of the node to check against
         * @throws JSONException If the JSON array contains anything other than a JSON object or the class type does not
         *                       match the type of the JSON "id" node
         */
        public static <T> List<T> extractNodeValuesFromJsonObjects(JSONArray jsonArray, String key)
                throws JSONException {
            List<T> idList = new ArrayList<>();
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObje = jsonArray.getJSONObject(i);
                idList.add((T) jsonObje.get(key));
            }
            return idList;
        }

    }

}
