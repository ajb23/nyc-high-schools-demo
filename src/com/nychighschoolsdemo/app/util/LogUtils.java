package com.nychighschoolsdemo.app.util;

import java.io.IOException;

public class LogUtils {

    public static void writeLogsToFile(String filePath) {
        try {
            Runtime.getRuntime().exec(new String[]{"logcat", "-f", filePath, "*:E"});
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
