package com.nychighschoolsdemo.app.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import com.nychighschoolsdemo.app.base.Application;

/**
 * Networking utility class.
 */
public class AppNetworkUtils {

    public static boolean connectedToInternet() {
        ConnectivityManager connectivityManager = (ConnectivityManager) Application.getContext().getSystemService(
                Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

}
