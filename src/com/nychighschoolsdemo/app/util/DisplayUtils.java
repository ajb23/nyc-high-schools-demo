package com.nychighschoolsdemo.app.util;

import android.app.Activity;
import android.graphics.Point;
import android.util.DisplayMetrics;
import com.nychighschoolsdemo.app.base.Application;

public class DisplayUtils {
    private static final DisplayMetrics DISPLAY_METRICS;
    private static final float PXLS_PER_ONE_DP;
    private static Point sScreenSizePoint;

    static {
        DISPLAY_METRICS = Application.getContext().getResources().getDisplayMetrics();
        // The scale of one dp to pixels on the users screen (1, 1.5, 2, 2.5 etc.)
        PXLS_PER_ONE_DP = (DISPLAY_METRICS.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
    }

    private DisplayUtils() {}

    public static DisplayMetrics getDisplayMetrics() {
        return DISPLAY_METRICS;
    }

    public static float getDensity() {
        return DISPLAY_METRICS.density;
    }

    public static int dpToPixels(int dp) {
        return Math.round(dp * DISPLAY_METRICS.density); // Note: return type is int since pixels are in whole numbers
    }

    public static int pxlsToDp(int pxls) {
        return Math.round(pxls / PXLS_PER_ONE_DP); // Note: return type is int since dens-ind pixels are in whole numbers
    }

    /**
     * Get the screen width a height.
     * @return A Point representing the screen width and height
     */
    public static Point getScreenWidthAndHeight(Activity actv) {
        if (sScreenSizePoint == null) {
            Point size = new Point();
            actv.getWindowManager().getDefaultDisplay().getSize(size);
            sScreenSizePoint = size;
        }
        return sScreenSizePoint;
    }

}
