package com.nychighschoolsdemo.app.util;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * Serialize utilities
 */
public class SerializeUtils {
    private static final Logger LOGGER = LoggerFactory.getLogger(SerializeUtils.class);
    public static final ObjectMapper DEFAULT_SERIALIZE_OBJECT_MAPPER;
    public static final ObjectMapper DEFAULT_DESERIALIZE_OBJECT_MAPPER;

    static {
        DEFAULT_SERIALIZE_OBJECT_MAPPER = new ObjectMapper();
        DEFAULT_DESERIALIZE_OBJECT_MAPPER = new ObjectMapper();

        // Omit unset properties in Json
        DEFAULT_SERIALIZE_OBJECT_MAPPER.setSerializationInclusion(JsonInclude.Include.NON_NULL);

        DEFAULT_DESERIALIZE_OBJECT_MAPPER.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, true);
        DEFAULT_DESERIALIZE_OBJECT_MAPPER.registerModule(new SimpleModule().setDeserializerModifier(
                new LowercaseEnumDeserializerModifier()));
    }

    /**
     * Serializes an object to Json
     */
    public static String serialize(Object object, ObjectMapper objectMapper) {
        String serl = null;
        try {
            serl = objectMapper.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return serl;
    }

    public static String serialize(Object object) {
        return serialize(object, DEFAULT_SERIALIZE_OBJECT_MAPPER);
    }

    /**
     * Deserializes any json to the given class type
     * @throws IOException If deserialization failed
     */
    public static <T> T deserialize(String jsonStr, Class<T> classType, ObjectMapper objectMapper) throws IOException {
        return objectMapper.readValue(jsonStr, classType);
    }

    public static <T> T deserialize(String jsonStr, Class<T> classType) throws IOException {
        return deserialize(jsonStr, classType, DEFAULT_DESERIALIZE_OBJECT_MAPPER);
    }

    public static <T> T deserialize(JSONObject json, Class<T> classType, ObjectMapper objectMapper) throws IOException {
        return deserialize(json.toString(), classType, objectMapper);
    }

    public static <T> T deserialize(JSONObject json, Class<T> classType) throws IOException {
        return deserialize(json.toString(), classType, DEFAULT_DESERIALIZE_OBJECT_MAPPER);
    }

}
