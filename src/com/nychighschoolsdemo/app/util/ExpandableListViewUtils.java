package com.nychighschoolsdemo.app.util;

import android.util.SparseArray;
import android.view.View;
import android.widget.ExpandableListView;

import java.util.ArrayList;
import java.util.List;

/**
 * ExpandableListView utilities
 */
public class ExpandableListViewUtils {

    /**
     * Get the group view of an ExpandableListView.
     */
    public static View getGroupView(ExpandableListView listView, int groupPosition) {
        long packedPosition = ExpandableListView.getPackedPositionForGroup(groupPosition);
        int flatPosition = listView.getFlatListPosition(packedPosition);
        int first = listView.getFirstVisiblePosition();
        return listView.getChildAt(flatPosition - first);
    }

    /**
     * Get the first visible group position of an ExpandableListView.
     */
    public static int getFirstVisibleGroupPosition(ExpandableListView expandableListView) {
        int firstVisbPost = expandableListView.getFirstVisiblePosition();
        long packdPost = expandableListView.getExpandableListPosition(firstVisbPost);
        return ExpandableListView.getPackedPositionGroup(packdPost);
    }

    /**
     * Get the visible expanded group positions of an ExpandableListView.
     * @return An integer array of visible group positions
     */
    public static int[] getVisibleExpandedGroupPositions(ExpandableListView expandableListView) {
        SparseArray<List<Integer>> groupPost_ChildPostList_SprsArray = getVisibleExpandedGroupAndChildPositions(
                expandableListView);
        int[] groupPosts = new int[groupPost_ChildPostList_SprsArray.size()];
        for (int i = 0; i < groupPost_ChildPostList_SprsArray.size(); i++) {
            groupPosts[i] = groupPost_ChildPostList_SprsArray.keyAt(i);
        }
        return groupPosts;
    }

    /**
     * Get the visible expanded group and child positions of an ExpandableListView.
     * @return A SparseArray with the key as the group position and value as a list of child positions
     */
    public static SparseArray<List<Integer>> getVisibleExpandedGroupAndChildPositions(
            ExpandableListView expandableListView) {
        int firstVisbPost  = expandableListView.getFirstVisiblePosition();
        int lastVisbPost = expandableListView.getLastVisiblePosition();
        int count = firstVisbPost;
        SparseArray<List<Integer>> groupPost_ChildPostList_SprsArray = new SparseArray<>();
        while (count <= lastVisbPost) {
            long expnListPost = expandableListView.getExpandableListPosition(count);
            if (ExpandableListView.getPackedPositionType(expnListPost) ==
                    ExpandableListView.PACKED_POSITION_TYPE_CHILD) {
                int groupPost = ExpandableListView.getPackedPositionGroup(expnListPost);
                int childPost = ExpandableListView.getPackedPositionChild(expnListPost);
                List<Integer> childPostList = groupPost_ChildPostList_SprsArray.get(groupPost);
                if (childPostList == null) {
                    childPostList = new ArrayList<>();
                    groupPost_ChildPostList_SprsArray.put(groupPost, childPostList);
                }
                childPostList.add(childPost);
            }
            count++;
        }
        return groupPost_ChildPostList_SprsArray;
    }

    /**
     * Collapse all the groups of an ExpandableListView.
     */
    public static void collapseAllGroups(ExpandableListView expandableListView) {
        int groupCount = expandableListView.getAdapter().getCount();
        for (int i = 0; i < groupCount; i++) {
            expandableListView.collapseGroup(i);
        }
    }

}
