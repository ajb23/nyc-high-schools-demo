package com.nychighschoolsdemo.app.util;

import android.view.View;

/**
 * Screen metrics utilities
 */
public class MetricsUtils {

    /**
     * Determine if the given coordinates are within the region of the view's boundaries.
     */
    public static boolean inRegion(float x, float y, View view) {
        int[] screenLoct = new int[] { 0, 0 };
        view.getLocationOnScreen(screenLoct);
        //DevLogger.logNoHeader("Check: "+y+" > "+(screenLoct[1] + view.getPaddingTop())+": "+String.valueOf(y > screenLoct[1]) + " | " + y +" < "+(screenLoct[1] + view.getHeight())+": "+String.valueOf(y < (screenLoct[1] + view.getHeight())), true);
        float paddngTop = view.getPaddingTop();
        return y > screenLoct[1] + paddngTop && // top edge
                y < screenLoct[1] + view.getHeight() + paddngTop + view.getPaddingBottom() && // bottom edge
                x > screenLoct[0] && // left edge
                x < screenLoct[0] + view.getWidth(); // right edge
    }

}
