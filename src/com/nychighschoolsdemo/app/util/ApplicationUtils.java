package com.nychighschoolsdemo.app.util;

import android.content.pm.PackageManager;
import com.nychighschoolsdemo.app.base.Application;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Application utilities
 */
public class ApplicationUtils {
    private static final Logger LOGGER = LoggerFactory.getLogger(ApplicationUtils.class);

    public static int getApplicationVersionCode() {
        try {
            return Application.getContext().getPackageManager().getPackageInfo(
                    Application.getContext().getPackageName(), 0).versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            LOGGER.error("Failed to get application version: {}", e.toString());
        }
        return -1;
    }

}
